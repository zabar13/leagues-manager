import React from 'react';
import { shallow } from 'enzyme';

import { CreateLeague } from "../../../src/front/components/options/CreateLeague";

test('renders correctly', () => {
    const tree = shallow(<CreateLeague teams={[]}/>);
    expect(tree).toMatchSnapshot();
  })