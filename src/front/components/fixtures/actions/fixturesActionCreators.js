import { ACTIONS } from '../reducers';

export function setFixturesState(matches) {
    return {
        type: ACTIONS.SET_STATE,
        matches
    }
}

export function changeMatchday(matchday) {
    console.log(matchday)
    return {
        type: ACTIONS.CHANGE_MATCHDAY,
        matchday
    };
}

export function setAvailableMatchdays(availableMatchdays) {
    return {
        type: ACTIONS.SET_AVAILABLE_MATCHDAYS,
        availableMatchdays
    }
}