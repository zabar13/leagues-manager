export const ACTIONS = {
    OPEN_EDIT_MATCH_DIALOG: Symbol('OPEN_EDIT_MATCH_DIALOG'),
    RESET_EDIT_MATCH_DIALOG: Symbol('RESET_EDIT_MATCH_DIALOG'),
    UPDATE_STAT: Symbol('UPDATE_STAT'),
    ADD_PLAYER: Symbol('ADD_PLAYER'),
    DELETE_PLAYER: Symbol('DELETE_PLAYER'),
    CHANGE_PLAYER_STATS: Symbol('CHANGE_PLAYER_STATS'),
    RESET_PLAYER: Symbol('RESET_PLAYER')
};

const DEFAULT_STATE = {
    open: false,
    match: {},
    scorer: {},
    booked: {}
};

export default function editMatchReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.OPEN_EDIT_MATCH_DIALOG:
            return {
                ...state,
                open: true,
                match: actions.match
            };
        case ACTIONS.ADD_PLAYER:
            return {
                ...state,
                match: {
                    ...state.match,
                    [actions.eventType]: [
                        ...state.match[actions.eventType],
                        actions.player]
                }
            };
        case ACTIONS.DELETE_PLAYER:
            return {
                ...state,
                match: {
                    ...state.match,
                    [actions.eventType]: [...state.match[actions.eventType]
                        .filter(player => player.customId
                            ? player.customId !== actions.customId
                            : player.id !== actions.customId)]
                }
            };
        case ACTIONS.CHANGE_PLAYER_STATS:
            return {
                ...state,
                [actions.eventType]: {
                    ...state[actions.eventType],
                    [actions.stat]: actions.value
                }
            };
        case ACTIONS.UPDATE_STAT:
            return {
                ...state,
                match: {
                    ...state.match,
                    [actions.side]: {
                        ...state.match[actions.side],
                        [actions.stat]: actions.value
                    }
                }
            };
        case ACTIONS.RESET_PLAYER:
            return {
                ...state,
                [actions.eventType]: {}
            };
        case ACTIONS.RESET_EDIT_MATCH_DIALOG:
            return DEFAULT_STATE;
        default:
            return state
    };
}

