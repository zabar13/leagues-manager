import React from 'react';
import { shallow } from 'enzyme';

import { Scorers } from '../../../src/front/components/scorers/Scorers';

test('renders correctly', () => {
    const tree = shallow(<Scorers scorers={[]} classes={{}}/>);
    expect(tree).toMatchSnapshot();
  })