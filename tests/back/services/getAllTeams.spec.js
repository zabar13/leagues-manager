import getAllTeams from '../../../src/back/services/getAllTeams';
import db from '../../../src/server/config';

jest.mock('../../../src/server/config');

beforeEach(() => {
    db.get.mockImplementationOnce(() => ({
        data: [{
            id: 7,
            name: 'Bye',
            season: 'Season test',
            short: 'BYE',
            teamNo: 0
        },
        {
            id: 8,
            name: 'Test_1',
            season: 'Season test',
            short: 'TE1',
            teamNo: 4
        }, {
            id: 3,
            name: 'Test_2',
            season: 'Season test',
            short: 'TE2',
            teamNo: 9
        }]
    }
    ))
});

test('should result equal expected with second param', async () => {

    expect.assertions(1);
    const results = await getAllTeams('Season test', 'All');

    const expected = ['All', 'Test_1', 'Test_2'];

    expect(results).toEqual(expected);
})

test('should result equal expected without second param', async () => {

    expect.assertions(1);
    const results = await getAllTeams('Season test');

    const expected = [{
        id: 7,
        name: 'Bye',
        season: 'Season test',
        short: 'BYE',
        teamNo: 0
    },
    {
        id: 8,
        name: 'Test_1',
        season: 'Season test',
        short: 'TE1',
        teamNo: 4
    }, {
        id: 3,
        name: 'Test_2',
        season: 'Season test',
        short: 'TE2',
        teamNo: 9
    }];

    expect(results).toEqual(expected);
})