import setFixturesInDB from "../../../../back/services/setFixturesinDB";
import { resetCreateSeason } from "./optionsActionsCreators";

export function createNewSeason(seasonData) {
    return async dispatch => {
        await setFixturesInDB(seasonData);
        dispatch(resetCreateSeason());
    }
}