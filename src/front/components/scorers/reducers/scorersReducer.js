export const ACTIONS = {
    SET_SCORERS: Symbol('SET_SCORERS'),
    CHANGE_TEAM: Symbol('CHANGE_TEAM'),
    SET_TEAMS: Symbol('SET_TEAMS')
};

const DEFAULT_STATE = {
    team: 'All',
    scorers: [],
    teams: []
};

export default function scorersReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.SET_SCORERS:
            return {
                ...state,
                scorers: actions.scorers
            };
        case ACTIONS.SET_TEAMS:
            return {
                ...state,
                teams: actions.teams
            };
        case ACTIONS.CHANGE_TEAM:
            return {
                ...state,
                team:actions.team
            };
        default:
            return state
    };
}