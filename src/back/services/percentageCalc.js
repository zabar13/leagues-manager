export default function percentageCalc(home, away) {

    const all = Number(home) + Number(away);

    const homePercentage = (100 * Number(home) / all).toFixed(2);
    const awayPercentage = (100 * Number(away) / all).toFixed(2);

    return [`${homePercentage}%`, `${awayPercentage}%`];
}