import React from 'react';
import { connect } from 'react-redux';
import { Button, Paper, TextField, FormControl, InputLabel, NativeSelect, Input } from '@material-ui/core';

import { ROUNDS, ADD_TEAM_FIELDS } from '../../../consts/const.json';
import { changeTeamData, resetTeam, addTeam, deleteTeam } from './actions/optionsActionsCreators';
import { createNewSeason } from './actions/optionActions';

const SHORT_NAME = 3, LONG_NAME = 30;

export function CreateLeague({ ...props }) {
    
    const onChange = (e, field) => {
        if (field === 'short') e.value = e.value.toUpperCase()
        props.changeTeamData(e.value, field)
    };

    const onAddClick = async () => {
        const { name, short, addTeam, resetTeam } = props;
        const id = Date.now();
        addTeam({ name, short: short.toUpperCase(), id });
        resetTeam();
    };

    const onCreateClick = async () => {
        const { teams, rounds, leagueName, createNewSeason, user } = props;
        createNewSeason({ teams, rounds, leagueName, user });
    };

    const { teams, rounds, leagueName, name, short } = props;

    return (
        <div>
            <div > CREATE NEW LEAGUE</div>
            <div>
                <div>
                    <Paper >LEAGUE NAME</Paper>
                    <Paper  >
                        <TextField
                            id="leagueName"
                            type="string"
                            margin="none"
                            value={leagueName ? leagueName : ''}
                            onChange={(e) => onChange(e.target, 'leagueName')} />
                    </Paper>

                    <Button
                        disabled={((rounds === 'None') || !leagueName || (teams.length < 2)) ? true : false}
                        onClick={onCreateClick}
                        variant="contained">
                        CREATE LEAGUE
                            </Button>
                </div >
                <FormControl>
                    <Paper  >NO. OF ROUNDS</Paper>
                    <NativeSelect
                        value={rounds}
                        onChange={(e) => onChange(e.target, 'rounds')}
                        input={<Input name="rounds" id="filled-season" />}
                    >
                        {ROUNDS.map((round, i) => {
                            return (
                                <option key={i} value={round}>{round}</option>
                            )
                        })}
                    </NativeSelect>
                </FormControl>
                <div>
                    <Paper >ADD TEAMS</Paper>
                    <div>
                        {ADD_TEAM_FIELDS.map((stat, i) => {
                            const maxlength = stat === 'short' ? SHORT_NAME : LONG_NAME;
                            const paperClassNames = i === 0 ? "default-paper" : "default-paper small-paper";
                            return (
                                <Paper key={i} className={paperClassNames}>
                                    <TextField
                                        InputProps={{
                                            disableUnderline: true,
                                        }}
                                        inputProps={{ maxLength: maxlength }}
                                        id={stat}
                                        type="string"
                                        value={props[stat] ? props[stat] : ''}
                                        onChange={(e) => onChange(e.target, stat)} />
                                </Paper>
                            )
                        })}
                        <Button
                            variant="contained"
                            onClick={onAddClick}
                            disabled={!name || !short}>Add</Button>
                    </div>
                    {teams.map((team, i) => {
                        return (
                            <div key={i}>
                                <Paper >{team.name}</Paper>
                                <Paper >{team.short}</Paper>
                                <Button
                                    onClick={() => props.deleteTeam(team.id)}>DELETE</Button>
                            </div>
                        )
                    })}
                </div>

            </div>
        </div>
    );
}

export default connect(state => {
    return {
        ...state.optionsReducer,
        user: state.loginReducer.user
    }
},
    { changeTeamData, addTeam, resetTeam, createNewSeason, deleteTeam })(CreateLeague)