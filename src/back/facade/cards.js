import models from '../../database/models';

export default {
    createCards: ({ body }, response) => {
        models.Cards.bulkCreate(body)
            .then(task => response.json(task));
    },

    deleteCard: ({ params }, response) => {
        const { id } = params;
        return models.Cards.destroy({
            where: {
                id
            }
        }).then(task => response.json(task));
    },

    findAllCards: ({ params }, response) => {
        const { season } = params;
        return models.Cards.findAll({
            where: { season }
        }).then(cards => {
            response.json(cards);
        });
    },
};