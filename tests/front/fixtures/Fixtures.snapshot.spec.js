import React from 'react';
import { shallow } from 'enzyme';

import { Fixtures } from '../../../src/front/components/fixtures/Fixtures';

test('renders correctly', () => {
    const tree = shallow(<Fixtures matches={[{ homeTeam: '', awayTeam: '', matchNo: 11 }]} classes={{}}/>);
    expect(tree).toMatchSnapshot();
})

Fixtures