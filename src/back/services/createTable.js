import _ from 'lodash'

import getSeasonData from './getSeasonData';

export default async function createTable(season, matchday) {
    const seasonData = await getSeasonData({season, matchday});

    const alreadyPlayed = seasonData.filter(item => item.goalsScored !== null);

    const teams = _.uniq(alreadyPlayed.map(team => {
        return team.teamName
    }).filter(team => team !== 'Bye'));

    const tableData = teams.map(team => {
        const allTeamEntry = alreadyPlayed.filter(elem => elem.teamName === team);
        return calculateTeamData(team, allTeamEntry)
    });
    return _.sortBy(tableData, ['points', 'goalsScored', 'goalsDifference']).reverse();
}

function calculateTeamData(team, allTeamEntry) {
    const statsNames = ['goalsScored', 'goalsLost', 'points'];

    const [goalsScored, goalsLost, points] = statsNames.map(stat => {
        return allTeamEntry.map(entry =>
            entry[stat]).reduce((a, b) => { return a + b; }, 0);
    })

    const matchPlayed = (allTeamEntry.filter(entry => entry.goalsScored !== null)).length;
    const matchWon = (allTeamEntry.filter(entry => entry.points == 3)).length;
    const matchDraw = (allTeamEntry.filter(entry => entry.points == 1)).length;
    const matchLost = (allTeamEntry.filter(entry => entry.points == 0)).length;

    const teamTable = {
        matchPlayed,
        team,
        points,
        matchWon,
        matchDraw,
        matchLost,
        goalsScored,
        goalsLost,
        goalsDifference: goalsScored - goalsLost
    };

    return teamTable;
}

