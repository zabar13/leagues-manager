import axios from 'axios';

const NODE_PORT = process.env.NODE_PORT || '8000';
const NODE_HOST = process.env.NODE_HOST || 'localhost';

const SERVER_URL = `http://${NODE_HOST}:${NODE_PORT}`;

export default axios.create({
        baseURL: SERVER_URL
    });