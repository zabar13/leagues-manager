import React from 'react';
import { shallow } from 'enzyme';

import { Login } from "../../../src/front/components/login/Login";

test('renders correctly', () => {
    const tree = shallow(<Login classes={{}}/>);
    expect(tree).toMatchSnapshot();
  })