import React from 'react';
import { Paper, Button, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
        '& >div': {
            margin: '10px'
        }
    },
    small: {
        width: '10%'
    },

    large: {
        width: '40%',

        '& >div': {
            width: '100%'
        }
    }
};

export function AddEventPlayer(props) {

    const { player, stats, classes } = props;

    const onChange = (e, stat) => {
        if (stat === 'nationality') e.value = e.value.toUpperCase();
        props.onChange(e.value, stat);
    }

    const onAddClick = () => {
        const customId = Date.now();
        props.add({ ...player, customId });
        props.reset();
    }

    

    return (
        <div className={classes.root}>
            {stats.map((stat, i) => {
                const textFieldType = (i === 0) ? 'number' : 'string';
                const paperClassNames = i === 1 ? classes.large : classes.small;
                return (
                    <Paper key={i} className={paperClassNames}>
                        <TextField
                            id={stat}
                            type={textFieldType}
                            value={player[stat] ? player[stat] : ''}
                            onChange={(e) => onChange(e.target, stat)} />
                    </Paper>
                )
            })}
            <Button
                onClick={onAddClick}>Add</Button>
        </div>
    );
}

export default withStyles(styles)(AddEventPlayer)