import deepFreeze from 'deep-freeze';

import fixtureReducer, { ACTIONS } from '../../../../src/front/components/fixtures/reducers';

test('should set available matchdays', () => {
    const stateBefore = {
        matchday: 'matchday1',
        matches: [],
        availableMatchdays: ['matchday1']
    };

    const action = {
        type: ACTIONS.SET_AVAILABLE_MATCHDAYS,
        availableMatchdays: ['matchday1', 'matchday2', 'matchday3']
    };

    const stateAfter = {
        matchday: 'matchday1',
        matches: [],
        availableMatchdays: ['matchday1', 'matchday2', 'matchday3']
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(fixtureReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change matchday', () => {
    const stateBefore = {
        matchday: 'matchday1',
        matches: [],
        availableMatchdays: ['matchday1', 'matchday2', 'matchday3']
    };

    const action = {
        type: ACTIONS.CHANGE_MATCHDAY,
        matchday: 'matchday3'
    };

    const stateAfter = {
        matchday: 'matchday3',
        matches: [],
        availableMatchdays: ['matchday1', 'matchday2', 'matchday3']
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(fixtureReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set matches', () => {
    const stateBefore = {
        matchday: 'matchday1',
        matches: [],
        availableMatchdays: ['matchday1']
    };

    const action = {
        type: ACTIONS.SET_STATE,
        matches: [
            { matchNo: 1, homeTeam: 'Team_1', awayTeam: 'Team_2' },
            { matchNo: 4, homeTeam: 'Team_3', awayTeam: 'Team_4' }
        ]
    };

    const stateAfter = {
        matchday: 'matchday1',
        matches: [
            { matchNo: 1, homeTeam: 'Team_1', awayTeam: 'Team_2' },
            { matchNo: 4, homeTeam: 'Team_3', awayTeam: 'Team_4' }
        ],
        availableMatchdays: ['matchday1']
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(fixtureReducer(stateBefore, action)).toEqual(stateAfter);
})