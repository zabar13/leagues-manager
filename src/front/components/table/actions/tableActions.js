import createTable from "../../../../back/services/createTable";
import {
    setTeams, openStatsDialog, setCompletedMatchdays,
    openChartDialog, openResultsDialog
} from "./tableActionsCreators";
import getTeamStats from "../../../../back/services/getTeamStats";
import findCompletedMatchdays from "../../../../back/services/findCompletedMatchdays";
import getTeamPositions from "../../../../back/services/getTeamPositions";
import getTeamsResults from "../../../../back/services/getTeamResults";

export function setTableState(season, matchday) {
    return async dispatch => {
        const teams = await createTable(season, matchday);
        const teamsNames = teams.map(team => team.team).sort();
        dispatch(setTeams(teams, ['None', ...teamsNames]))
    };
}

export function setMatchdays(season) {
    return async dispatch => {
        const matchdays = await findCompletedMatchdays(season);
        dispatch(setCompletedMatchdays(matchdays))
    }
}

export function setchoosenTeamData(choosenTeam, season, matchday) {
    return async dispatch => {
        const teamData = await getTeamStats({ choosenTeam, season, matchday });
        dispatch(openStatsDialog(teamData));
    };
}

export function setChoosenTeamPositions({ choosenTeam, season, matchday }) {
    return async dispatch => {
        const teamPositions = await getTeamPositions({ choosenTeam, season, matchday });
        dispatch(openChartDialog(teamPositions));
    }
}

export function setChoosenTeamResults({ choosenTeam, season, matchday }) {
    return async dispatch => {
        const teamResults = await getTeamsResults({ choosenTeam, season, matchday });
        dispatch(openResultsDialog(teamResults));
    }
}

export function countMatchdaysToGenerate({ noOfMatchdays, teamPositions }) {
    const matchdaysToGenerate = noOfMatchdays - teamPositions.length;

    let allMatchdays = [...teamPositions];

    if (matchdaysToGenerate > 0) {
        for (let i = 1; i < matchdaysToGenerate; i++) {
            allMatchdays = [...allMatchdays, { matchday: `${i + teamPositions.length}` }];
        };
    }

    return allMatchdays;
}

export function checkMatchBackgroundcolour(match, choosenTeam) {
    const { home, homeGoals, awayGoals } = match;
    let colour;

    if (home === choosenTeam) {
        if (homeGoals > awayGoals) {
            colour = 'green';
        } else if (homeGoals < awayGoals) {
            colour = 'red';
        } else {
            colour = 'yellow'
        }
    } else {
        if (homeGoals < awayGoals) {
            colour = 'green';
        } else if (homeGoals > awayGoals) {
            colour = 'red';
        } else {
            colour = 'yellow'
        }
    }

    return colour;
}