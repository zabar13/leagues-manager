import db from '../../server/config';
import getSeasonData from './getSeasonData';

export default async function getTeamsResults({ choosenTeam, season, matchday }) {

    const allTeamData = await getSeasonData({ season, matchday });
    const playedMatches = allTeamData.filter(data => data.points !== null);

    const teamMatches = playedMatches
        .filter(match => match.teamName === choosenTeam)
        .map(match => {
            const { matchday, matchNo } = match;
            return { matchday, matchNo };
        });

    return teamMatches.map(match => {
        const completeMatchData = playedMatches
            .filter(playedMatch => playedMatch.matchday === match.matchday
                && playedMatch.matchNo === match.matchNo);

        if (completeMatchData.length < 2) {
            return;
        }

        return {
            matchday: completeMatchData[0].matchday.replace('matchday', ''),
            home: completeMatchData[0].teamName,
            homeGoals: completeMatchData[0].goalsScored,
            away: completeMatchData[1].teamName,
            awayGoals: completeMatchData[1].goalsScored,
        };
    }).sort((a,b) => a.matchday - b.matchday);
}

