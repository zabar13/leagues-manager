import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { setTableState, setchoosenTeamData, setMatchdays, } from './actions/tableActions';
import TeamStats from './TeamStats';
import { setPickedTeam, setMatchday } from './actions/tableActionsCreators';
import { TABLE_HEADER_LABELS, TABLE_STATS_NAMES } from '../../../consts/const.json';
import CustomSelect from '../common/CustomSelect';
import TeamPositionsChart from './TeamPositionsChart';
import TeamResults from './TeamResults';

const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        justifyContent: 'space-around',
        padding: '10px',

        '& >:first-child': {
            display: 'flex',
            flexDirection: 'column',

            '& >:first-child': {
                marginBottom: '20px'
            }
        },

        '& >:nth-child(2)': {
            width: '70%',
            overflow: 'auto',

            '& >div': {
                display: 'flex',
            }
        },
    },

    paper: {
        textAlign: 'center',
        fontSize: '16px',
        margin: '5px',
        width: '8%',
    },

    paperTeamName: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        textAlign: 'center',
        fontSize: '16px',
        margin: '5px',
        width: '24%'
    },

    clickable: {
        cursor: 'pointer',
    }
}

export function Table(props) {

    const { setTableState, setMatchdays, chosenSeason, matchday,
        teams, teamsNames, pickedTeam, classes, matchdays } = props;

    useEffect(() => {
        setMatchdays(chosenSeason);
        setTableState(chosenSeason, matchday);
    }, [matchday,pickedTeam]);


    const onTeamClick = (team) => {
        if (team.matchPlayed !== 0)
            props.setchoosenTeamData(team.team, chosenSeason, matchday);
    }

    return (
        <div className={classes.root}>
            <div>
                <CustomSelect
                    label='HIGHLIGHT TEAM'
                    onChangeFunction={(selected) => props.setPickedTeam(selected)}
                    dataArray={teamsNames}
                    value={pickedTeam} />
                <CustomSelect
                    label='TABLE AFTER'
                    onChangeFunction={(selected) => props.setMatchday(selected)}
                    dataArray={matchdays}
                    value={matchday} />
            </div>
            <div>
                <div>
                    {TABLE_HEADER_LABELS.map((label, i) => {
                        if (i === 1) {
                            return <Paper key={i} className={classes.paperTeamName} >{label}</Paper>
                        }
                        return <Paper key={i} className={classes.paper} >{label}</Paper>
                    })}
                </div>
                {teams.map((team, i) => {
                    const highlightedTeam = (team.team === pickedTeam) ? "#F9D22B" : ""
                    return (
                        <div className={classes.clickable}
                            key={i}
                            onClick={() => onTeamClick(team)}>
                            <Paper className={classes.paper} style={{ backgroundColor: highlightedTeam }} > {i + 1}</Paper>
                            {TABLE_STATS_NAMES.map((stat, i) => {
                                if (stat === "team") {
                                    return <Paper key={i}
                                        className={classes.paperTeamName}
                                        style={{ backgroundColor: highlightedTeam }}
                                    >{team[stat]}</Paper>
                                }
                                return <Paper key={i}
                                    className={classes.paper}
                                    style={{ backgroundColor: highlightedTeam }}
                                >{team[stat]}</Paper>
                            })}
                        </div>
                    )
                })
                }
                <TeamStats />
                <TeamPositionsChart />
                <TeamResults />
            </div >
        </div>
    );
};

const mapStateToProps = state => {
    return {
        ...state.table,
        chosenSeason: state.mainReducer.chosenSeason
    };
};

const mapDispatchToProps = {
    setTableState,
    setchoosenTeamData, setPickedTeam, setMatchdays, setMatchday
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(Table)))