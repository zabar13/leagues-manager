import _ from 'lodash';

import db from '../../server/config';
import { TEAM_STATS } from '../../consts/const.json';

export default async function getTeamStats({ choosenTeam, season, matchday }) {
    const tempMatchday = matchday ? matchday : 'Total';
    const { data: allTeamData } = await db.get(`api/fixtures/get/team/${choosenTeam}/${season}/${tempMatchday}`);
    const playedMatches = allTeamData.filter(data => data.points !== null);
    const matchPlayed = playedMatches.length;
    const teamsSummedStats = avgTeamStats(playedMatches, matchPlayed);


    return {
        choosenTeam,
        matchPlayed,
        ...teamsSummedStats
    };
}

function avgTeamStats(matches, matchPlayed) {
    let stats = {};
    TEAM_STATS.map(stat => {
        stats = {
            ...stats,
            [stat]: avgStat({ matches, stat, matchPlayed })
        };
    });

    return stats
}

function avgStat({ matches, stat, matchPlayed }) {
    const statList = matches.map(match => match[stat]);
    const statSum = statList.reduce((a, b) => { return a + b; }, 0);
    return roundToTwo(statSum / matchPlayed);
}

function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}