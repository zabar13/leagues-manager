import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import percentageCalc from '../../../back/services/percentageCalc';


const styles = {
    root: {
        width: '100%',
        height: '8px',
        display: 'flex',
        flexDirection: 'row',
        margin: '0 10px',

        '& :first-child': {
            backgroundColor: 'lightcoral',
        },

        '& :nth-child(2)': {
            backgroundColor: 'lightgreen',
        }
    }
}

export function StatsVisuals({ home, away, classes }) {

    const [homePercentage, awayPercentage] = percentageCalc(home, away);

    return (
        <div className={classes.root}>
            <div style={{ width: homePercentage }} > </div>
            <div style={{ width: awayPercentage }} > </div>
        </div >
    );
}

export default withStyles(styles)(StatsVisuals)