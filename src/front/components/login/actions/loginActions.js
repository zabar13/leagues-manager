import findUserinDB from "../../../../back/services/findUserinDB";
import { changeLogStatus, resetLogin ,setUser} from "./loginActionsCreators";

export function tryToLog(credentials) {
    return async dispatch => {
        const user = await findUserinDB(credentials);
        if (user.length > 0) {
            dispatch(setUser(user[0]));
            dispatch(changeLogStatus(true));
            dispatch(resetLogin());
            return true;
        }
        return false
    };
}