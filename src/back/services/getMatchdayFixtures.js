import _ from 'lodash';

import db from '../../server/config';

export default async function getMatchdayFixtures(matchday,season) {
    const { data } = await db.get(`api/fixtures/get/matchday/${matchday}/${season}`);
    let matchNos = _.uniq(data.map((item) => { return item.matchNo }))
    return _.orderBy(matchNos.map(matchNo => {
        const [homeTeam,awayTeam] = data.filter(item => item.matchNo === matchNo);
        return {
            matchNo,
            homeTeam,
            awayTeam,
            scorers: [...homeTeam.scorers,...awayTeam.scorers],
            cards:[...homeTeam.cards,...awayTeam.cards]
        };
    }),['matchNo']);
}