import getSeasonData from "./getSeasonData";
import sortMatchdays from "./sortMatchdays";

export default async function findCompletedMatchdays(season) {
    const seasonData = await getSeasonData({ season, played: true });

    const sorted = sortMatchdays(seasonData);

    return ['Total', ...sorted.splice(0, sorted.length - 1)];
}