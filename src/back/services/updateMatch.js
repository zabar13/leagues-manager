import db from '../../server/config';

export default async function updateMatch(match,season,teams) {
    const { homeTeam, awayTeam } = match;
    const [homeScorers, awayScorers] = mapScorers(match, 'scorers',teams);
    const [homeCards, awayCards] = mapScorers(match, 'cards',teams);
    const { homeTeamPoints, awayTeamPoints } = points(homeTeam.goalsScored, awayTeam.goalsScored);

    const updateList = [
        {
            ...homeTeam,
            goalsLost: awayTeam.goalsScored,
            points: homeTeamPoints,
            scorers: filterStats(homeScorers, homeTeam.id,season),
            cards: filterStats(homeCards, homeTeam.id,season)
        },
        {
            ...awayTeam,
            goalsLost: homeTeam.goalsScored,
            points: awayTeamPoints,
            scorers: filterStats(awayScorers, awayTeam.id,season),
            cards: filterStats(awayCards, awayTeam.id,season)
        }
    ];

    for (let i = 0; i < updateList.length; i++) {
        await db.post(`api/matches/${updateList[i].id}`, updateList[i]);
        await db.post('api/scorers', updateList[i].scorers);
        await db.post('api/cards', updateList[i].cards);
    };

    return true;
}

function points(homeGoals, awayGoals) {
    switch (true) {
        case (homeGoals == awayGoals):
            return {
                homeTeamPoints: 1,
                awayTeamPoints: 1
            };
        case (homeGoals > awayGoals):
            return {
                homeTeamPoints: 3,
                awayTeamPoints: 0
            };
        case (homeGoals < awayGoals):
            return {
                homeTeamPoints: 0,
                awayTeamPoints: 3
            };
        default: return;
    };
}

function mapScorers(match, type,teams) {
    const { scorers, cards } = match;
    const stats = type === 'cards' ? cards : scorers;

    if (stats.length === 0) {
        return [[], []]
    }

    const { homeTeam, awayTeam } = match;
    const teamsCodes = [homeTeam.teamName, awayTeam.teamName]
        .map(name => {
            return (teams.filter((team) => team.name == name))[0].short;
        });

    return teamsCodes.map(teamCode => {
        return stats.filter(stat => stat.nationality == teamCode);
    });

}

function filterStats(players, id, season) {
    return players.filter(player => !player.id)
        .map(player => {
            return { ...player, SeasonId: id,season }
        })
}