import models from '../../database/models';

export default {
    createScorers: ({ body }, response) => {
        return models.Scorers.bulkCreate(body)
            .then(task => response.json(task));
    },

    deleteScorer: ({ params }, response) => {
        const { id } = params;
        return models.Scorers.destroy({
            where: {
                id
            }
        }).then(task => response.json(task));
    },

    findSeasonScorersByNationality: ({ params }, response) => {
        const { nationality, season } = params;
        return models.Scorers.findAll({
            where: {
                nationality,
                season
            }
        }).then(fixtures => {
            response.json(fixtures);
        });
    },

    findSeasonScorers: ({ params }, response) => {
        const { season } = params;

        return models.Scorers.findAll({
            where: {
                season
            }
        }).then(fixtures => {
            response.json(fixtures);
        });
    },
    countGoals: ({ params }, response) => {
        const { season } = params;
        return models.Scorers.count({
            where: {
                season
            }
        }).then(counted => {
            response.json(counted)
        })
    }
};