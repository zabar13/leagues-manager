import getScorers from '../../../src/back/services/getScorers';

import db from '../../../src/server/config';

jest.mock('../../../src/server/config');

const teams = [{
    season: 'Season test',
    name: 'test_1',
    short: 'TE1',
    teamNo: 2
}, {
    season: 'Season test',
    name: 'test_2',
    short: 'TE2',
    teamNo: 4
}, {
    season: 'Season test',
    name: 'test_3',
    short: 'TE3',
    teamNo: 5
}];


test('should scorer list equal expected with team !== All ', async () => {
    db.get.mockImplementationOnce(() => ({
        data: [{
            id: 1,
            name: 'J.Bona',
            nationality: 'TE1',
            season: 'Season test',
            time: 89
        }, {
            id: 56,
            name: 'J.Bona',
            nationality: 'TE1',
            season: 'Season test',
            time: 16
        }, {
            id: 14,
            name: 'R.Testy',
            nationality: 'TE1',
            season: 'Season test',
            time: 56
        }, {
            id: 1,
            name: 'G.Racki',
            nationality: 'TE1',
            season: 'Season test',
            time: 39
        },]
    }));

    expect.assertions(1);

    const scorers = await getScorers('test_1', 'Season test', teams);

    const expected = [
        [2, [{
            goals: 2,
            name: 'J.Bona',
            nationality: 'TE1',
        }]
        ],
        [1, [{
            goals: 1,
            name: 'R.Testy',
            nationality: 'TE1',
        }, {
            goals: 1,
            name: 'G.Racki',
            nationality: 'TE1',
        }]
        ]
    ];

    expect(scorers).toEqual(expected);
})

test('should scorer list equal expected with team === All ', async () => {
    db.get.mockImplementationOnce(() => ({
        data: [{
            id: 1,
            name: 'J.Bona',
            nationality: 'TE1',
            season: 'Season test',
            time: 89
        }, {
            id: 56,
            name: 'J.Bona',
            nationality: 'TE1',
            season: 'Season test',
            time: 16
        }, {
            id: 14,
            name: 'R.Testy',
            nationality: 'TE1',
            season: 'Season test',
            time: 56
        }, {
            id: 6,
            name: 'G.Racki',
            nationality: 'TE2',
            season: 'Season test',
            time: 39
        },{
            id: 78,
            name: 'G.Racki',
            nationality: 'TE2',
            season: 'Season test',
            time: 12
        },{
            id: 63,
            name: 'G.Racki',
            nationality: 'TE2',
            season: 'Season test',
            time: 59
        },{
            id: 10,
            name: 'L.Tubis',
            nationality: 'TE3',
            season: 'Season test',
            time: 39
        },]
    }
    ))
    expect.assertions(1);

    const scorers = await getScorers('test_1', 'Season test', teams);

    const expected = [
        [3,[{
            goals: 3,
            name: 'G.Racki',
            nationality: 'TE2'
        }]],
        [2, [{
            goals: 2,
            name: 'J.Bona',
            nationality: 'TE1',
        }]
        ],
        [1, [{
            goals: 1,
            name: 'R.Testy',
            nationality: 'TE1',
        }, {
            goals: 1,
            name: 'L.Tubis',
            nationality: 'TE3',
        }]
        ]
    ];

    expect(scorers).toEqual(expected);
})