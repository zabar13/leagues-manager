import getAvailableSeasons from "../../../../back/services/getAvailableSeasons";
import { setAvailableSeasons, setSeason } from "./mainActionsCreators";
import getSeasonsTeams from "../../../../back/services/getSeasonsTeams";
import countSeasonStats from "../../../../back/services/countSeasonStats";

export function findAvailableSeasons() {
    return async dispatch => {
        const availableSeasons = await getAvailableSeasons();
        dispatch(setAvailableSeasons(availableSeasons))
    };
}

export function setSeasonData(chosenSeason) {
    return async dispatch => {
        const teams = await getSeasonsTeams(chosenSeason);
        const seasonStats = await countSeasonStats(chosenSeason);
        dispatch(setSeason({chosenSeason, teams, seasonStats}));
    };
}