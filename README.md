After clone repository to add all dependencies type

-- `npm i` 
 
then 
-- `npm start` 

to run app and create new database.

If you want use example database 

(you better do it because the option to create your own league has not yet worked :) )

rename file "db_dev_example.db" to "db_dev.db" 
and then 

--- `npm start` .

#### ABOUT THE PROJECT

The project started when I started to wonder which national team in Fifa 17 is the best.

I wondered who would win the league in which everyone would play with everyone.

(Match results are simulated by exibition match in FIFA game)

And because I like numbers and statistics and didn't want to count them manually, 

I started to create a tool for it.

When I was in the process of programming, I discovered that maybe it could also be a tool

(for schools for example) to organize small football leagues.

In the future it should be an application to create and manage football leagues (maybe also other sports). 

It should also be possible to choose which statistics to display.(currently statistics are related to those from Fifa game)

#### NEWS

Currently, i'm working on: 

* refactoring css (material-ui withStyles added) and RWD (didn't start yet)
* creating logic for league statistics
* on the administrative part (new users, creating a new league from an excel file)
* fixing bugs if i find any.

New things will come .... maybe soon.