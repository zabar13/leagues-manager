import {ACTIONS} from '../reducers';

export function openMatchStatsDialog(match) {
    return {
        type: ACTIONS.OPEN_MATCH_STATS_DIALOG,
        match
    };
}

export function resetMatchStatsDialog() {
    return {
        type: ACTIONS.RESET_MATCH_STATS_DIALOG
    };
}