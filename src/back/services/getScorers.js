import _ from 'lodash';

import db from '../../server/config';

export default async function getScorers(team, season, teams) {
    let dataScorers;
    if (team !== 'All') {
        const nationality = mapTeamName(team, teams);
        const { data } = await db.get(`api/scorers/get/${nationality}/${season}`);
        dataScorers = data;
    } else {
        const { data } = await db.get(`api/scorers/all/${season}`);
        dataScorers = data;
    }

    const scorers = deletePenalty(dataScorers);

    const uniqueScorers = getUniqueScorers(scorers);

    const scorersData = deleteOwnGoals(
        mapScorers(scorers, uniqueScorers));
    const goalsNo = mapGoalsNo(scorersData).sort((a, b) => a - b);


    return goalsNo.map(goal => {
        const scorers = scorersData.filter(scorer => scorer.goals === goal)
        return [goal, scorers]
    }).reverse()

}

function deleteOwnGoals(scorers) {
    return scorers.filter(scorer => !((scorer.name).includes('own')));
}

function deletePenalty(scorers) {
    return scorers.map(scorer => {
        let { name } = scorer;
        name = name.replace('(pen)', '').trim();
        return {
            ...scorer,
            name
        }
    })
}

function getUniqueScorers(scorers) {
    const scorersMap = scorers.map(scorer => {
        const { name, nationality } = scorer;
        return { name, nationality };
    });

    return _.uniqWith(scorersMap, _.isEqual);
}

function mapScorers(scorers, uniqueScorers) {
    return uniqueScorers.map(scorer => {
        const { name, nationality } = scorer;
        const goals = scorers.filter(scores => scores.name === name && scores.nationality === nationality).length;
        return {
            nationality,
            name,
            goals
        }
    })
}

function mapGoalsNo(goals) {
    return _.uniq(goals.map(goal => goal.goals));
}

function mapTeamName(teamName, teams) {
    return teams.filter(team => team.name === teamName)[0].short;
}
