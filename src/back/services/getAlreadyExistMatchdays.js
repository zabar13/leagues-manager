import _ from 'lodash';

import db from '../../server/config';
import getSeasonData from './getSeasonData';
import sortMatchdays from './sortMatchdays';

export default async function getAlreadyExistMatchdays(season) {
    let matchdays;
    if (!season) {
        const { data } = await db.get('api/fixtures/get');
        matchdays = data;
    } else {
        const data  = await getSeasonData({season});
        matchdays = data;
    };
    
    return sortMatchdays(matchdays);
}

