import React from 'react';
import { shallow } from 'enzyme';

import { Table } from "../../../src/front/components/table/Table";

test('renders correctly', () => {
  const tree = shallow(<Table teams={[]} teamsNames={[]} classes={{}} />);
  expect(tree).toMatchSnapshot();
})