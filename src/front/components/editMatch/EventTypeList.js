import React, { Fragment } from 'react';
import { Paper, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    eventElementContainer: {
        justifyContent: 'center',

        '& div': {
            fontSize: '12px',
            width: '10%',
            textAlign: 'center',
            margin: '2px 10px'
        },

        '& :nth-child(2)': {
            width: '40%'
        },

        '& button': {
            fontSize: '8px',
            padding: '0',
        }
    }
};

export function EventTypeList({ match, eventType, onDeleteClick, classes }) {
    return (
        <Fragment>
            {match[eventType].map((event, i) => {
                let cellColour = 'white';
                if (eventType === 'cards') {
                    cellColour = event.colour;
                }
                return (
                    <div className={classes.eventElementContainer} key={i}>
                        <Paper style={{ backgroundColor: cellColour }}>{event.time}</Paper>
                        <Paper >{event.name}</Paper>
                        <Paper >{event.nationality}</Paper>
                        <Button
                            onClick={() => onDeleteClick(event, eventType)}>
                            DELETE</Button>
                    </div>
                )
            })}
        </Fragment>
    );
}

export default withStyles(styles)(EventTypeList)