import React from 'react';
import { connect } from 'react-redux';
import { Paper, TextField, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        flexDirection: 'column',
        padding: '20px 0',

        '& >div:first-child': {
            textAlign: 'center',
            marginBottom: '20px'
        },

        '& >div:nth-child(2)': {
            display: 'flex',
            justifyContent: 'space-around',

            '& div': {
                width: '20%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',

                '& >div': {
                    width: '100%'
                }
            }
        },
    }
};

export function Users({ classes }) {
    return (
        <div className={classes.root}>
            <div> ADD NEW USER</div>
            <div>
                <Paper >LOGIN</Paper>
                <Paper >
                    <TextField
                        id="login"
                        type="string" />
                </Paper>
                <Paper >PASSWORD</Paper>
                <Paper >
                    <TextField
                        id="password"
                        type="string" />
                </Paper>
                <Button
                    variant="contained"
                    size="small"
                >ADD</Button>
            </div>
        </div>
    )
}

const mapStateToProps = state => state.mainReducer;

export default connect(mapStateToProps)((withStyles(styles)(Users)))