export function createMatchdaysArray(matchday) {

    if (matchday === "Total") {
        return [];
    }

    const matchdayNo = matchday.replace('matchday', '');
    let matchdays = [];

    for (let i = 1; i <= matchdayNo; i++) {
        matchdays = [...matchdays, `matchday${i}`]
    };

    return matchdays;
}