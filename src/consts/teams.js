export const teams = [
    {
        teamNo: '(bye)',
        name: 'Bye',
        short: 'BYE'
    },
    {
        teamNo: 1,
        name: 'Bolivia',
        short: 'BOL'
    },
    {
        teamNo: 2,
        name: 'Argentina',
        short: 'ARG'
    },
    {
        teamNo: 3,
        name: 'Australia',
        short: 'AUS'
    },
    {
        teamNo: 4,
        name: 'Austria',
        short: 'AUT'
    },
    {
        teamNo: 5,
        name: 'Belgium',
        short: 'BEL'
    },
    {
        teamNo: 6,
        name: 'Brazil',
        short: 'BRA'
    },
    {
        teamNo: 7,
        name: 'Bulgaria',
        short: 'BUL'
    },
    {
        teamNo: 8,
        name: 'Cameroon',
        short: 'CMR'
    },
    {
        teamNo: 9,
        name: 'Canada',
        short: 'CAN'
    },
    {
        teamNo: 10,
        name: 'Chile',
        short: 'CHI'
    },
    {
        teamNo: 11,
        name: 'China',
        short: 'CHN'
    },
    {
        teamNo: 12,
        name: 'Colombia',
        short: 'COL'
    },
    {
        teamNo: 13,
        name: 'Czech Republic',
        short: 'CZE'
    },
    {
        teamNo: 14,
        name: 'Denmark',
        short: 'DEN'
    },
    {
        teamNo: 15,
        name: 'England',
        short: 'ENG'
    },
    {
        teamNo: 16,
        name: 'France',
        short: 'FRA'
    },
    {
        teamNo: 17,
        name: 'Germany',
        short: 'GER'
    },
    {
        teamNo: 18,
        name: 'Greece',
        short: 'GRE'
    },
    {
        teamNo: 19,
        name: 'Netherlands',
        short: 'NED'
    },
    {
        teamNo: 20,
        name: 'Ecuador',
        short: 'ECU'
    },
    {
        teamNo: 21,
        name: 'Hungary',
        short: 'HUN'
    },
    {
        teamNo: 22,
        name: 'Egypt',
        short: 'EGY'
    },
    {
        teamNo: 23,
        name: 'Finland',
        short: 'FIN'
    },
    {
        teamNo: 24,
        name: 'Italy',
        short: 'ITA'
    },
    {
        teamNo: 25,
        name: 'Ivory Coast',
        short: 'CIV'
    },
    {
        teamNo: 26,
        name: 'India',
        short: 'IND'
    },
    {
        teamNo: 27,
        name: 'Paraguay',
        short: 'PAR'
    },
    {
        teamNo: 28,
        name: 'Mexico',
        short: 'MEX'
    },
    {
        teamNo: 29,
        name: 'Peru',
        short: 'PER'
    },
    {
        teamNo: 30,
        name: 'Slovenia',
        short: 'SLO'
    },
    {
        teamNo: 31,
        name: 'South Africa',
        short: 'RPA'
    },
    {
        teamNo: 32,
        name: 'North Ireland',
        short: 'NIR'
    },
    {
        teamNo: 33,
        name: 'Norway',
        short: 'NOR'
    },
    {
        teamNo: 34,
        name: 'Poland',
        short: 'POL'
    },
    {
        teamNo: 35,
        name: 'Portugal',
        short: 'POR'
    },
    {
        teamNo: 36,
        name: 'Venezuela',
        short: 'VEN'
    },
    {
        teamNo: 37,
        name: 'Ireland',
        short: 'IRL'
    },
    {
        teamNo: 38,
        name: 'Romania',
        short: 'ROU'
    },
    {
        teamNo: 39,
        name: 'Russia',
        short: 'RUS'
    },
    {
        teamNo: 40,
        name: 'Scotland',
        short: 'SCO'
    },
    {
        teamNo: 41,
        name: 'Spain',
        short: 'ESP'
    },
    {
        teamNo: 42,
        name: 'Sweden',
        short: 'SWE'
    },
    {
        teamNo: 43,
        name: 'Switzerland',
        short: 'SUI'
    },
    {
        teamNo: 44,
        name: 'Turkey',
        short: 'TUR'
    },
    {
        teamNo: 45,
        name: 'United States',
        short: 'USA'
    },
    {
        teamNo: 46,
        name: 'Uruguay',
        short: 'URU'
    },
    {
        teamNo: 47,
        name: 'Wales',
        short: 'WAL'
    },
    {
        teamNo: 48,
        name: 'Croatia',
        short: 'CRO'
    },
    {
        teamNo: 49,
        name: 'South Korea',
        short: 'KOR'
    },
    {
        teamNo: 50,
        name: 'New Zealand',
        short: 'NZL'
    }
];