import db from '../../server/config';

export default function deletePlayerFromDB(player, event) {
    db.delete(`api/${event}/delete/${player.id}`);
    
}
