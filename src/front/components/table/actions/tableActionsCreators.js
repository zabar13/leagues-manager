import { ACTIONS } from '../reducers';

export function setTeams(teams,teamsNames) {
    return {
        type: ACTIONS.SET_TEAMS,
        teams,
        teamsNames
    };
}

export function setPickedTeam(pickedTeam) {
    return {
        type: ACTIONS.SET_PICKED_TEAM,
        pickedTeam
    };
}

export function setCompletedMatchdays(matchdays) {
    return {
        type: ACTIONS.SET_MATCHDAYS,
        matchdays
    };
}

export function setMatchday(matchday) {
    return {
        type: ACTIONS.SET_MATCHDAY,
        matchday
    };
}

export function openStatsDialog(choosenTeam) {
    return {
        type: ACTIONS.OPEN_STATS_DIALOG,
        choosenTeam
    };
}

export function closeStatsDialog() {
    return {
        type: ACTIONS.CLOSE_STATS_DIALOG
    };
}

export function openChartDialog(teamPositions) {
    return {
        type: ACTIONS.OPEN_CHART_DIALOG,
        teamPositions
    };
}

export function closeChartDialog() {
    return {
        type: ACTIONS.CLOSE_CHART_DIALOG
    };
}

export function openResultsDialog(teamResults) {
    return {
        type: ACTIONS.OPEN_RESULTS_DIALOG,
        teamResults
    };
}

export function closeResultsDialog() {
    return {
        type: ACTIONS.CLOSE_RESULTS_DIALOG
    };
}
openResultsDialog