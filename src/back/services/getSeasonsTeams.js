import db from '../../server/config';

export default async function getSeasonsTeams(season) {
    const {data} = await db.get(`api/teams/${season}`);
    
    return data;
}