import { ACTIONS } from '../reducers';

export function openEditMatchDialog(match) {
    return {
        type: ACTIONS.OPEN_EDIT_MATCH_DIALOG,
        match
    };
}

export function resetEditMatchDialog() {
    return {
        type: ACTIONS.RESET_EDIT_MATCH_DIALOG
    };
}

export function changeStats(value, side, stat) {
    return {
        type: ACTIONS.UPDATE_STAT,
        value,
        side,
        stat
    };
}

export function addPlayer(player, eventType) {
    return {
        type: ACTIONS.ADD_PLAYER,
        player,
        eventType
    };
}

export function deletePlayer(customId, eventType) {
    return {
        type: ACTIONS.DELETE_PLAYER,
        customId,
        eventType
    };
}

export function changePlayerStats(value, stat, eventType) {
    return {
        type: ACTIONS.CHANGE_PLAYER_STATS,
        value,
        stat,
        eventType
    };
}

export function resetPlayer(eventType) {
    return {
        type: ACTIONS.RESET_PLAYER,
        eventType
    };
}