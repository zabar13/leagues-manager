import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import MatchStats from '../matchStats/MatchStats';
import EditMatch from '../editMatch/EditMatch';
import { findSelectedMatchday, setAllAvailableMatchdays } from './actions/fixturesActions';
import { changeMatchday } from './actions/fixturesActionCreators';
import { openEditMatchDialog } from '../editMatch/actions/editMatchActionsCreators';
import { openMatchStatsDialog } from '../matchStats/actions/matchStatsActionCreators';
import CustomSelect from '../common/CustomSelect';

const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        justifyContent: 'space-around',
        padding: '10px',

        '& >:nth-child(2)': {
            display: 'inherit',
            flexDirection: 'column',
            width: '70%',
            marginRight: '30px',
            textAlign: 'center',
            overflow: 'auto',

            '& >div': {
                display: 'flex',
                '& div': {
                    fontSize: '18px',
                    margin: '5px',
                },

                '& div:first-child, & div:nth-child(3), & div:nth-child(4)': {
                    width: '10%'
                },

                '& div:nth-child(2), & div:nth-child(5)': {
                    width: '30%',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                },

                '& button': {
                    margin: '5px',
                    padding: '0'
                }

            }
        }
    },

    hidden: {
        display: 'none'
    }
};

export function Fixtures(props) {

    const {
        matchday, matches, availableMatchdays, chosenSeason, seasonAllTeams, logged, classes,
        openEditMatchDialog, openMatchStatsDialog, changeMatchday
    } = props;

    useEffect(() => {
        props.setAllAvailableMatchdays(chosenSeason);
        props.findSelectedMatchday(matchday, chosenSeason);
    }, [matchday]);


    return (
        <div className={classes.root}>
            <CustomSelect
                label='SELECT MATCHDAY'
                onChangeFunction={(selected) => changeMatchday(selected)}
                dataArray={availableMatchdays}
                value={matchday}
            />
            <div>
                {matches.map(match => {
                    const { homeTeam, awayTeam } = match;
                    if (homeTeam.teamName !== 'Bye' && awayTeam.teamName !== 'Bye') {
                        return (
                            <div key={match.matchNo}>
                                <Paper >{match.matchNo}</Paper>
                                <Paper >{homeTeam.teamName}</Paper>
                                <Paper >{homeTeam.goalsScored}</Paper>
                                <Paper >{awayTeam.goalsScored}</Paper>
                                <Paper >{awayTeam.teamName}</Paper>
                                <Button
                                    variant='contained'
                                    size="small"
                                    disabled={homeTeam.goalsScored !== null ? false : true}
                                    onClick={() => openMatchStatsDialog(match)}
                                >STATS</Button>
                                <Button
                                    className={!logged ? classes.hidden : ''}
                                    variant='contained'
                                    size="small"
                                    onClick={() => openEditMatchDialog(match)}
                                >EDIT</Button>
                            </div>
                        )
                    }
                })}
            </div>
            <MatchStats />
            <EditMatch matchday={matchday} season={chosenSeason} teams={seasonAllTeams} />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        ...state.fixtures,
        chosenSeason: state.mainReducer.chosenSeason,
        seasonAllTeams: state.mainReducer.teams,
        logged: state.loginReducer.logged
    }
};

const mapDispatchToProps = {
    findSelectedMatchday, changeMatchday,
    openEditMatchDialog, setAllAvailableMatchdays,
    openMatchStatsDialog
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(Fixtures)))