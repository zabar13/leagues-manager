import getMatchdayFixtures from "../../../../back/services/getMatchdayFixtures";
import { setFixturesState, setAvailableMatchdays } from "./fixturesActionCreators";
import updateMatch from "../../../../back/services/updateMatch";
import getAlreadyExistMatchdays from "../../../../back/services/getAlreadyExistMatchdays";

export function findSelectedMatchday(matchday,season) {
    return async dispatch => {
        const matches = await getMatchdayFixtures(matchday,season);
        dispatch(setFixturesState(matches));
    };
}

export function updateFixtures({ match, matchday,season,teams }) {
    return async dispatch => {
        await updateMatch(match,season,teams);
        dispatch(findSelectedMatchday(matchday,season));
    };
}

export function setAllAvailableMatchdays(season) {
    return async dispatch => {
        const availableMatchdays = await getAlreadyExistMatchdays(season);
        dispatch(setAvailableMatchdays(availableMatchdays));
    }
}
