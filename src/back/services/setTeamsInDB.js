import db from '../../server/config';

export default async function setTeamsInDB(teams, leagueName) {
    const mappedTeams = mapTeams(teams, leagueName);

    return await db.post('api/teams', mappedTeams);
};



function mapTeams(teams, season) {
    const mappedTeams = teams.map((team, i) => {
        const { name, short } = team;
        return { name, short, teamNo: i + 1, season }
    });

    return [{
        teamNo: 0,
        name: 'Bye',
        short: 'BYE',
        season
    },
    ...mappedTeams
    ]
}