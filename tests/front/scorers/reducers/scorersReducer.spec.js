import deepFreeze from 'deep-freeze';

import scorersReducer, { ACTIONS } from '../../../../src/front/components/scorers/reducers/scorersReducer';

test('should set scorers', () => {
    const stateBefore = {
        team: 'All',
        scorers: [],
        teams: []
    };

    const action = {
        type: ACTIONS.SET_SCORERS,
        scorers: [
            { name: 'J.Loki', nationality: 'TE1' },
            { name: 'K.Thor', nationality: 'TE2' }]
    };

    const stateAfter = {
        team: 'All',
        scorers: [
            { name: 'J.Loki', nationality: 'TE1' },
            { name: 'K.Thor', nationality: 'TE2' }],
        teams: []
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(scorersReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set available teams', () => {
    const stateBefore = {
        team: 'All',
        scorers: [],
        teams: []
    };

    const action = {
        type: ACTIONS.SET_TEAMS,
        teams: ['Test_1', 'Test_2', 'Test_3']
    };

    const stateAfter = {
        team: 'All',
        scorers: [],
        teams: ['Test_1', 'Test_2', 'Test_3']
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(scorersReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set team', () => {
    const stateBefore = {
        team: 'All',
        scorers: [
            { name: 'J.Loki', nationality: 'TE1' },
            { name: 'K.Thor', nationality: 'TE2' }],
        teams: ['Test_1', 'Test_2', 'Test_3']
    };

    const action = {
        type: ACTIONS.CHANGE_TEAM,
        team: 'Test_2'
    };

    const stateAfter = {
        team: 'Test_2',
        scorers: [
            { name: 'J.Loki', nationality: 'TE1' },
            { name: 'K.Thor', nationality: 'TE2' }
        ],
        teams: ['Test_1', 'Test_2', 'Test_3']
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(scorersReducer(stateBefore, action)).toEqual(stateAfter);
})