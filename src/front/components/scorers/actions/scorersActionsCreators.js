import { ACTIONS } from '../reducers/scorersReducer';

export function setScorers(scorers) {
    return {
        type:ACTIONS.SET_SCORERS,
        scorers
    };
}

export function changeTeam(team) {
    return {
        type:ACTIONS.CHANGE_TEAM,
        team
    };
}

export function setTeams(teams) {
    return {
        type:ACTIONS.SET_TEAMS,
        teams
    };
}