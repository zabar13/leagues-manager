import _ from 'lodash';

import db from '../../server/config';
import getSeasonData from './getSeasonData';

const NO_TEAMS_IN_MATCH = 2;

export default async function countSeasonStats(season) {
    const seasonData = await getSeasonData({ season });
    const noOfMatchdays = _.uniq(seasonData.map(match => match.matchday)).length;
    const { data: goalsScored } = await db.get(`/api/scorers/count/${season}`);
    const { data: allCards } = await db.get(`/api/cards/${season}`);

    const byeMatches = seasonData.filter(match => match.teamName === 'Bye').length;
    const allMatches = (seasonData.length - (byeMatches * NO_TEAMS_IN_MATCH)) / NO_TEAMS_IN_MATCH;
    const playedMatches = seasonData.filter(match => match.goalsScored !== null).length / NO_TEAMS_IN_MATCH;
    const avgGoals = playedMatches != 0 ? (goalsScored / playedMatches).toFixed(2) : 0;
    const yellowCards = allCards.filter(card => card.colour === "Yellow").length;
    const avgYellowCards = playedMatches != 0 ? (yellowCards / playedMatches).toFixed(2) : 0;
    const redCards = allCards.filter(card => card.colour === "Red").length;
    const avgRedCards = playedMatches != 0 ? (redCards / playedMatches).toFixed(2) : 0;

    return {
        allMatches,
        playedMatches,
        goalsScored,
        avgGoals,
        yellowCards,
        avgYellowCards,
        avgRedCards,
        redCards,
        noOfMatchdays
    };
}