import _ from 'lodash';

import db from '../../server/config';
import createFixturesObject from './createFixturesObject';

export default async function setFixturesInDB(season) {
    const fixtures = await createFixturesObject(season);

    if (fixtures) {
        const chunkedFixtures = _.chunk(fixtures, 200);
        for (let i = 0; i < chunkedFixtures.length; i++) {
            db.post('api/fixtures/set', chunkedFixtures[i]);
        };
    };
}