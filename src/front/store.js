import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import mainReducer  from './components/main/reducers';
import loginReducer from './components/login/reducers/loginReducer';
import optionsReducer from './components/options/reducers/optionsReducer';
import fixtures from './components/fixtures/reducers';
import editMatch from '../front/components/editMatch/reducers';
import scorersReducer from './components/scorers/reducers/scorersReducer';
import table from './components/table/reducers';
import matchStats from './components/matchStats/reducers';

const store = createStore(combineReducers({
    mainReducer,
    loginReducer,
    optionsReducer,
    fixtures,
    editMatch,
    matchStats,
    scorersReducer,
    table
}), applyMiddleware(thunk));

export default store;