import findUserinDb from '../../../src/back/services/findUserinDb';
import db from '../../../src/server/config';

jest.mock('../../../src/server/config');

test('should founded user equal expected ', async () => {
    db.get.mockImplementationOnce(() => ({
        data: [{ login: 'test', password: 'test' }]
    }))

    expect.assertions(1);
    const results = await findUserinDb('test', 'test');

    const expected = [{ login: 'test', password: 'test' }];

    expect(results).toEqual(expected);
})