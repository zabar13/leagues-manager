export const ACTIONS = {
    OPEN_LOGIN: Symbol('OPEN_LOGIN'),
    CHANGE_USER_DATA: Symbol('CHANGE_DATA'),
    RESET_LOGIN: Symbol('RESET_LOGIN'),
    CHANGE_LOG_STATUS: Symbol('CHANGE_LOG_STATUS'),
    SET_ERROR: Symbol('SET_ERROR'),
    SET_USER: Symbol('SET_USER')
};

const DEFAULT_STATE = {
    open: false,
    logged: false
};

export default function loginReducer(state = DEFAULT_STATE,actions) {
    switch (actions.type) {
        case ACTIONS.OPEN_LOGIN:
            return {
                ...state,
                open: true
            };
        case ACTIONS.CHANGE_LOG_STATUS:
            return {
                ...state,
                logged: actions.logged
            };
        case ACTIONS.CHANGE_USER_DATA:
            return {
                ...state,
                [actions.data]: actions.value
            };
        case ACTIONS.RESET_LOGIN:
            return {
                ...DEFAULT_STATE,
                logged: state.logged,
                user:state.user
            };
        case ACTIONS.SET_ERROR:
            return {
                ...state,
                error: actions.error
            };
        case ACTIONS.SET_USER:
            return {
                ...state,
                user: actions.user
            };
        default:
            return state
    };
}