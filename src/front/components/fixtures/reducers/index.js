export const ACTIONS = {
    SET_STATE: Symbol('SET_STATE'),
    CHANGE_MATCHDAY: Symbol('CHANGE_MATCHDAY'),
    SET_AVAILABLE_MATCHDAYS: Symbol('SET_AVAILABLE_MATCHDAYS')
};

const DEFAULT_STATE = {
    matchday: 'matchday1',
    matches: [],
    availableMatchdays: ['matchday1']
};

export default function fixtureReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.SET_STATE:
            return {
                ...state,
                matches: actions.matches
            };
        case ACTIONS.CHANGE_MATCHDAY:
            return {
                ...state,
                matchday: actions.matchday
            };
        case ACTIONS.SET_AVAILABLE_MATCHDAYS:
            return {
                ...state,
                availableMatchdays: actions.availableMatchdays
            };
        default:
            return state
    };
}