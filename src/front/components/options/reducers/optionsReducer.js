export const ACTIONS = {
    UPDATE_TEAM_DATA: Symbol('UPDATE_TEAM_DATA'),
    RESET_TEAM: Symbol('RESET_TEAM'),
    ADD_TEAM: Symbol('ADD_TEAM'),
    RESET_CREATE_SEASON: Symbol('RESET_CREATE_SEASON'),
    DELETE_TEAM: Symbol('DELETE_TEAM')
};

const DEFAULT_STATE = {
    name: null,
    short: null,
    leagueName: '',
    teams: [],
    rounds: 'None'
};

export default function optionsReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.UPDATE_TEAM_DATA:
            return {
                ...state,
                [actions.field]: actions.value
            };
        case ACTIONS.RESET_TEAM:
            return {
                ...state,
                name: null,
                short: null
            };
        case ACTIONS.ADD_TEAM:
            return {
                ...state,
                teams: [...state.teams, actions.team]
            };
        case ACTIONS.DELETE_TEAM:
            return {
                ...state,
                teams: [...state.teams.filter(team => team.id !== actions.id)]
            };
        case ACTIONS.RESET_CREATE_SEASON:
            return DEFAULT_STATE;
        default:
            return state
    };
}