import models from '../../database/models';

export default {
    createTeams: ({ body }, response) => {
        return models.Teams.bulkCreate(body)
            .then(teams => response.json(teams));
    },

    findSeasonTeams: ({ params }, response) => {
        const { season } = params;
        season.replace(/%20 /g, ' ');
        return models.Teams.findAll({
            where: {
                season
            }
        }).then(fixtures => {
            response.json(fixtures);
        });
    }
};