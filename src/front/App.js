import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import "@babel/polyfill";

import Header from './components/Header';
import Menu from './components/Menu';
import Main from './components/main/Main';
import Fixtures from './components/fixtures/Fixtures';
import Table from './components/table/Table';
import Scorers from './components/scorers/Scorers';
import Options from './components/options/Options';
import Login from './components/login/Login';
import Users from './components/users/Users';

const styles = {
  root: {
    '& >div:nth-child(2)': {
      position: 'fixed',
      top: '100px',
      display: 'flex',
      flexDirection: 'row',
      height: 'calc(100vh - 100px)',
    }
  }
}

export function App({classes,...props}) {
    return (
      <div className={classes.root}>
        <Header />
        <div className="main">
          <Menu />
          <Switch>
            <Route
              path="/"
              exact
              component={Main} />
            <Route
              path="/fixtures"
              exact
              component={Fixtures} />
            <Route
              path="/table"
              exact
              component={Table} />
            <Route
              path="/scorers"
              exact
              component={Scorers} />
            <Route
              path="/options"
              exact
              component={Options} />
            <Route
              path="/users"
              exact
              component={Users} />
          </Switch>
          <Login />
        </div>
      </div>
    );
}

export default withStyles(styles)(App)