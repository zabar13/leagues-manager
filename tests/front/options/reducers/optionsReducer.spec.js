import deepFreeze from 'deep-freeze';

import optionsReducer, { ACTIONS } from '../../../../src/front/components/options/reducers/optionsReducer';

test('should change name', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.UPDATE_TEAM_DATA,
        value: 'Test',
        field: 'name'
    };

    const stateAfter = {
        name: 'Test',
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change short', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.UPDATE_TEAM_DATA,
        value: 'Test',
        field: 'short'
    };

    const stateAfter = {
        name: null,
        short: 'Test',
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change short', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.UPDATE_TEAM_DATA,
        value: '2_ROUNDS',
        field: 'rounds'
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: '2_ROUNDS'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})
test('should change short', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.UPDATE_TEAM_DATA,
        value: 'Season test',
        field: 'leagueName'
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: 'Season test',
        teams: [],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset team data', () => {
    const stateBefore = {
        name: 'Test',
        short: 'Test',
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.RESET_TEAM
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should add team', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [{ name: 'Test_1', short: 'TE1' }],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.ADD_TEAM,
        team: { name: 'Test_2', short: 'TE2' }
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: '',
        teams: [
            { name: 'Test_1', short: 'TE1' },
            { name: 'Test_2', short: 'TE2' }
        ],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should delete team', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: '',
        teams: [
            { id: 23, name: 'Test_1', short: 'TE1' },
            { id: 4, name: 'Test_2', short: 'TE2' }
        ],
        rounds: 'None'
    };

    const action = {
        type: ACTIONS.DELETE_TEAM,
        id: 23
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: '',
        teams: [
            { id: 4, name: 'Test_2', short: 'TE2' }
        ],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should rested state', () => {
    const stateBefore = {
        name: null,
        short: null,
        leagueName: 'Season x',
        teams: [
            { id: 23, name: 'Test_1', short: 'TE1' },
            { id: 4, name: 'Test_2', short: 'TE2' }
        ],
        rounds: '2_ROUNDS'
    };

    const action = {
        type: ACTIONS.RESET_CREATE_SEASON
    };

    const stateAfter = {
        name: null,
        short: null,
        leagueName: '',
        teams: [],
        rounds: 'None'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(optionsReducer(stateBefore, action)).toEqual(stateAfter);
})
