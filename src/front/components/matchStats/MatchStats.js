import React from 'react';
import { connect } from 'react-redux';
import {
    Button, Paper, Dialog, DialogActions, DialogContent, DialogTitle
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { resetMatchStatsDialog } from './actions/matchStatsActionCreators';
import { MATCH_TEAM_STATS, MATCH_STATS_LABELS } from '../../../consts/const.json'
import MatchStastPlayersEvents from './MatchStastPlayersEvents';
import StatsVisuals from './StatsVisuals';

const styles = {
    body: {
        '& >div>div:first-child': {
            display: 'flex',
            justifyContent: 'center',

            '& div': {
                margin: '5px 10px',
                fontSize: '32px',
                textAlign: 'center',
            },

            '& div:first-child, & div:nth-child(4)': {
                width: '30%',
            },

            '& div:nth-child(2), & div:nth-child(3)': {
                width: '10%',
            }
        },

        '& >div>div:nth-child(2)': {
            display: 'flex',
            justifyContent: 'center',

            '& div': {
                margin: '10px',
                fontSize: '16px',
                textAlign: 'center',
                width: '10%',
            },
        },


    },

    statContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

        '& >div': {
            display: 'inherit',
            flexDirection: 'column',
            alignItems: 'center',
            width: '100%',

            '& >div:first-child': {
                width: '20%',
                textAlign: 'center',
                margin: '5px'
            },


            '& >div:nth-child(2)': {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                width: '80%',
                textAlign: 'center',

                '& >div:first-child , &>div:nth-child(3)': {
                    width: '10%',
                }
            }
        },

    }
}

export function MatchStats({ open, match, resetMatchStatsDialog, classes }) {
    const { awayTeam, homeTeam } = match;

    let homeTeamHalfTimeGoals, awayTeamHalfTimeGoals;

    if (awayTeam && homeTeam) {
        [homeTeamHalfTimeGoals, awayTeamHalfTimeGoals] = [homeTeam, awayTeam]
            .map(team => {
                return team.scorers.filter(scorer => scorer.time <= 45).length;
            });
    }

    return (
        <Dialog
            className={classes.root}
            fullWidth
            maxWidth="lg"
            scroll="body"
            disableBackdropClick={true}
            open={open}
            onClose={() => resetMatchStatsDialog()}
        >
            <DialogTitle className={classes.bars}>
                MATCH STATISTICS
            </DialogTitle>
            <DialogContent className={classes.body}>
                {match.length !== 0 ?
                    <div>
                        <div >
                            <Paper >{homeTeam.teamName}</Paper>
                            <Paper >{homeTeam.goalsScored}</Paper>
                            <Paper >{awayTeam.goalsScored}</Paper>
                            <Paper >{awayTeam.teamName}</Paper>
                        </div>
                        {(homeTeam.goalsScored === 0 && awayTeam.goalsScored === 0 ? <div></div>
                            : <div >
                                <Paper >{homeTeamHalfTimeGoals}</Paper>
                                <Paper >{awayTeamHalfTimeGoals}</Paper>
                            </div>
                        )}
                        <MatchStastPlayersEvents homeTeam={homeTeam} awayTeam={awayTeam} events='scorers' />
                        <MatchStastPlayersEvents homeTeam={homeTeam} awayTeam={awayTeam} events='cards' />
                        <div className={classes.statContainer}>
                            {MATCH_TEAM_STATS.map((stat, i) => {
                                if (!match.homeTeam[stat] && !match.awayTeam[stat]) return
                                return (
                                    <div key={stat} >
                                        <Paper >{MATCH_STATS_LABELS[i]}</Paper>
                                        <div >
                                            <Paper >{match.homeTeam[stat]}</Paper>
                                            <StatsVisuals home={match.homeTeam[stat]} away={match.awayTeam[stat]} />
                                            <Paper >{match.awayTeam[stat]}</Paper>
                                        </div>
                                    </div>
                                )
                            })}

                        </div>
                    </div>
                    : <div>Empty</div>}

            </DialogContent>
            <DialogActions className={classes.bars}>
                <Button
                    variant='text'
                    onClick={resetMatchStatsDialog}
                    color='inherit'>
                    OK
            </Button>
            </DialogActions>
        </Dialog >
    );
}

const mapStatesToProps = state => state.matchStats;

const mapDispatchToProps = { resetMatchStatsDialog };

export default connect(mapStatesToProps, mapDispatchToProps)((withStyles(styles)(MatchStats)));