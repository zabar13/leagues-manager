import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

import { openLogin, changeLogStatus } from './login/actions/loginActionsCreators';

const styles = {
    header: {
        width: '100vw',
        position: 'fixed',
        backgroundColor: '#081E30',
        color: '#ffffff',
        height: '100px',
        textAlign: 'center',
        fontSize: '30px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',

        '& button': {
            color: '#ffffff'
        },

        '& div': {
            display: 'flex',
            flexDirection: 'column',
            fontSize: '14px'
        }
    }
};

export function Header({ chosenSeason, openLogin, logged, changeLogStatus, user, classes }) {

    return (
        <header className={classes.header}>
            <h3 >Leagues Manager </h3>
            <h3 >{chosenSeason !== 'None' ? chosenSeason : ''} </h3>

            {logged ? <div>
                Logged as {user.login}
                <Button onClick={() => changeLogStatus(false)}>Logout</Button>
            </div>
                : <Button onClick={() => openLogin()}>Login</Button>
            }

        </header>
    );
};

const mapStatesToProps = state => {
    return {
        chosenSeason: state.mainReducer.chosenSeason,
        ...state.loginReducer
    }
}

const mapDispatchToProps = {
    openLogin, changeLogStatus
}

export default connect(mapStatesToProps, mapDispatchToProps)((withStyles(styles)(Header)));