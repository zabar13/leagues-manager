import React from 'react';
import { connect } from 'react-redux';
import { DialogTitle, Dialog, DialogContent, DialogActions, Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid
} from 'recharts';

import { closeChartDialog } from './actions/tableActionsCreators';
import { countMatchdaysToGenerate } from './actions/tableActions';

const styles = {};


export function TeamPositionsChart({
    choosenTeam, teamPositions, openChart, closeChartDialog, teamsNames, noOfMatchdays }) {

    const allMatchdays = countMatchdaysToGenerate({ teamPositions, noOfMatchdays });
    const positionsArray = [...Array(teamsNames.length + 1).keys()];

    return (
        <Dialog
            fullWidth
            disableBackdropClick={true}
            open={openChart}
            onClose={closeChartDialog}
        >
            <DialogTitle>
                <div>{`${choosenTeam.choosenTeam} positions chart`}</div>
            </DialogTitle>
            <DialogContent >
                <LineChart
                    width={1000}
                    height={500}
                    data={allMatchdays}
                    margin={{
                        top: 5, right: 15, left: 5, bottom: 10,
                    }}
                >
                    <CartesianGrid />
                    <XAxis
                        dataKey="matchday"
                        label={{ value: 'Matchday', position: 'insideBottom', offset: -5 }}
                    />
                    <YAxis
                        reversed={true}
                        label={{ value: 'Position', angle: -90, position: 'insideLeft' }}
                        scale="linear"
                        ticks={positionsArray} />
                    <Line type="monotone" dataKey="position" stroke="#8884d8" activeDot={{ r: 5 }} />
                </LineChart>
            </DialogContent>
            <DialogActions>
                <Button
                    variant='text'
                    onClick={closeChartDialog}
                    color='inherit'>
                    OK
                    </Button>
            </DialogActions>
        </Dialog >
    );
}

const mapStateToProps = state => {
    return {
        ...state.table,
        noOfMatchdays: state.mainReducer.seasonStats.noOfMatchdays
    };
};

const mapDispatchToProps = {
    closeChartDialog
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(TeamPositionsChart)));