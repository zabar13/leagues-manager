import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core';

import './front/css/main.less';
import App from './front/App';
import store from './front/store';
import {theme} from './front/theme';

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <MemoryRouter>
                <App />
            </MemoryRouter>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
);