import React from 'react';
import { shallow } from 'enzyme';

import MatchStastPlayersEvents from '../../../src/front/components/matchStats/MatchStastPlayersEvents';

test('renders correctly', () => {
    const tree = shallow(<MatchStastPlayersEvents
        homeTeam={{ events: [{name:'',time:''}] }}
        awayTeam={{ events: [{name:'',time:''}] }}
        events={'events'} />);
    expect(tree).toMatchSnapshot();
})