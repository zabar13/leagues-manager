import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';

import { setSeason } from './actions/mainActionsCreators';
import { findAvailableSeasons, setSeasonData } from './actions/mainActions';
import CustomSelect from '../common/CustomSelect';


const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        justifyContent: 'space-around',
        padding: '20px',

        '& >:nth-child(2)': {
            display: 'inherit',
            flexDirection: 'column',
            width: '70%',
            marginRight: '30px',
            textAlign: 'center',

            '& >div': {
                display: 'inherit',
                justifyContent: 'space-between',
                margin: '10px 0',
                fontSize: '16px',

            },

            '& >div div:first-child': {
                width: '70%',
                padding: '5px',
            },

            '& >div div:nth-child(2)': {
                width: '10%',
                padding: '5px',
            }
        }
    }
}

export function Main({
    availableSeasons, chosenSeason, classes,
    seasonStats, setSeasonData, findAvailableSeasons, ...props
}) {

    useEffect(() => {
        findAvailableSeasons();
        setSeasonData(chosenSeason);
    },[]);

    return (
        <div className={classes.root}>
            <CustomSelect
                label='SELECT LEAGUE'
                onChangeFunction={(selected) => setSeasonData(selected)}
                dataArray={availableSeasons}
                value={chosenSeason} />
            <div className={classes.league}>
                <h3>LEAGUE STATISTICS</h3>
                <div>
                    <Paper>MATCH TO PLAY</Paper>
                    <Paper>{seasonStats.allMatches || 0}</Paper>
                </div>
                <div>
                    <Paper>MATCH ALREADY PLAYED</Paper>
                    <Paper>{seasonStats.playedMatches || 0}</Paper>
                </div>
                <div>
                    <Paper>GOALS SCORED</Paper>
                    <Paper>{seasonStats.goalsScored || 0}</Paper>
                </div>
                <div>
                    <Paper>AVG. GOALS ON MATCH</Paper>
                    <Paper>{seasonStats.avgGoals || 0}</Paper>
                </div>
                <div>
                    <Paper>YELLOW CARDS</Paper>
                    <Paper>{seasonStats.yellowCards || 0}</Paper>
                </div>
                <div>
                    <Paper>AVG. YELLOW CARDS ON MATCH</Paper>
                    <Paper>{seasonStats.avgYellowCards || 0}</Paper>
                </div>
                <div>
                    <Paper>RED CARDS</Paper>
                    <Paper>{seasonStats.redCards || 0}</Paper>
                </div>
                <div>
                    <Paper>AVG. RED CARDS ON MATCH</Paper>
                    <Paper>{seasonStats.avgRedCards || 0}</Paper>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = state => state.mainReducer;

const mapDispatchToProps = {
    setSeason, findAvailableSeasons, setSeasonData
}

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(Main)))