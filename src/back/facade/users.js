import models from '../../database/models';

export default {
    findUser: ({ params }, response) => {
        const { login, password } = params;
        models.Users.findAll({
            where: {
                login, password
            }
        }).then(users => {
            response.json(users);
        });
    }
};