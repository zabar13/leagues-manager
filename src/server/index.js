import express from 'express';
import bodyParser from 'body-parser';

import sequelize_fixtures from 'sequelize-fixtures';

import models from '../database/models';
import fixtures from '../database/fixtures/users.json';
import routes from '../back/routes';

const app = express();

const PORT = process.env.PORT || '8000';

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next();
});

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use('/', routes);

app.use(express.static('dist'));

models.sequelize.sync().then(() => {
    sequelize_fixtures.loadFixtures(fixtures, models).then(() => {
        app.listen(PORT, err => err
            ? console.error(err)
            : console.log(`It\'s ALIVE ON ${PORT} !!!`)
        );
    });

});