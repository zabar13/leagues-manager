import db from '../../server/config';

export default async function findUserinDB({ login, password }) {
    const { data: users } = await db.get(`api/users/find/${login}/${password}`);
    return users
}