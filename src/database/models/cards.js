'use strict';
module.exports = (sequelize, DataTypes) => {
    const Cards = sequelize.define('Cards', {
        season: DataTypes.STRING,
        time: DataTypes.INTEGER,
        name: DataTypes.STRING,
        colour: DataTypes.STRING,
        nationality: DataTypes.STRING,
    }, {});
    Cards.associate = function (models) {
        Cards.belongsTo(models.Season)
    };
    return Cards;
};