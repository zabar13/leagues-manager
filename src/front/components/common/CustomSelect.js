import React from 'react';
import { Paper, NativeSelect, Input, FormControl } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root : {
        '& div:first-child': {
            fontSize: '16px',
            padding:'5px 10px',
            textAlign:'center',
        }
    }
}

export function CustomSelect({ label, onChangeFunction, dataArray, value,classes }) {

    function handleChange(event) {
        onChangeFunction(event.target.value);
    };

    return (
        <FormControl className={classes.root}>
            <Paper>{label}</Paper>
            <NativeSelect
                value={value}
                onChange={handleChange}
                input={<Input name={label} />}
            >
                {dataArray.map((item, i) => {
                    return (
                        <option key={i} value={item}>{item}</option>
                    );
                }
                )}
            </NativeSelect>
        </FormControl>
    )
}

export default withStyles(styles)(CustomSelect)