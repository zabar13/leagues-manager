import models from '../../database/models';
import { createMatchdaysArray } from '../services/createMatchdaysArray';

export default {
    updateSeason: ({ body, params }, response) => {
        const { id } = params;
        return models.Season.update(body,
            {
                where: {
                    id
                }
            }).then(() => models.Season.findById(id,
                { raw: true })
                .then(data => response.json(data)));
    },

    findSeason: ({ params }, response) => {
        const { season, matchday, played } = params;
        const matchdays = createMatchdaysArray(matchday);

        return models.Season.findAll({
            where: {
                season,
                matchday: (matchdays.length > 0) ? { $in: matchdays } : { $not: null },
                goalsScored: (played === 'true') ? { $not: null } : { $or: [{ $not: null }, { $is: null }] },
            }
        }).then(fixtures => {
            response.json(fixtures);
        });
    },

    createSeason: ({ body }, response) => {
        return models.Season.bulkCreate(body)
            .then(task => response.json(task));
    },

    findSeasonTeam: ({ params }, response) => {
        const { teamName, season, matchday } = params;
        const matchdays = createMatchdaysArray(matchday);

        return models.Season.findAll({
            where: {
                teamName,
                season,
                matchday: (matchdays.length > 0) ? { $in: matchdays } : { $not: null }
            }
        }).then(fixtures => {
            response.json(fixtures);
        });
    },

    findSeasonMatchday: ({ params }, response) => {
        const { matchday, season } = params;

        return models.Season.findAll({
            where: {
                matchday, season
            },
            include: [{
                model: models.Scorers,
                as: 'scorers'
            },
            {
                model: models.Cards,
                as: 'cards'
            }
            ]
        }).then(fixtures => {
            response.json(fixtures);
        });
    },

    findAllSeasons: (response) => {
        return models.Season.findAll({
        }).then(fixtures => {
            response.json(fixtures);
        });
    },

    countPlayedMatches: ({ params }, response) => {
        const { season } = params;
        return models.Season.count({
            where: {
                season,
                goalsScored: { $not: null }
            }
        }).then(counted => {
            response.json(counted)
        })
    }
}