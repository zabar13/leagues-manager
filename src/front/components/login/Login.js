import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Dialog, DialogTitle, DialogContent, Paper, DialogActions, Button, Input, FormHelperText } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { LOGIN } from '../../../consts/const.json';
import { resetLogin, changeUserData, setError } from './actions/loginActionsCreators.js';
import { tryToLog } from './actions/loginActions.js';

const styles = {
    body: {
        '& >div>div>div:first-child': {
            textAlign: 'center',
            margin: '5px',
            padding: '3px',
        },

        '& >div>div:nth-child(2)>div:first-child': {
            marginTop: '20px'
        },
    }
}

export function Login(props) {

    const handleClose = () => {
        props.resetLogin();
    };

    const onChange = (e, dataType) => {
        props.changeUserData(e.value, dataType);
    };

    const onLoginClick = async () => {
        const { login, password, tryToLog } = props;
        const succes = await tryToLog({ login, password });
        if (!succes) props.setError("Invalid Credentials");
    };

    return (
        <Dialog
            maxWidth='md'
            disableBackdropClick={true}
            open={props.open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle>
                <div>LOGIN</div>
            </DialogTitle>
            <DialogContent className={props.classes.body}>
                <div>
                    {LOGIN.map(item => {
                        return (
                            <div key={item}>
                                <Paper>{item.toUpperCase()}</Paper>
                                <Paper>
                                    <Input
                                        id="login"
                                        type='string'
                                        autoComplete='off'
                                        value={props[item] ? props[item] : ''}
                                        onChange={(e) => onChange(e.target, item)} />
                                </Paper>
                                <FormHelperText >{props.error ? props.error : ''}</FormHelperText>
                            </div>
                        )
                    })}
                </div>

            </DialogContent>
            <DialogActions>
                <Button
                    onClick={onLoginClick}
                    color="inherit">
                    LOGIN
            </Button>
                <Button
                    color="inherit"
                    onClick={handleClose}>
                    CANCEL
            </Button>
            </DialogActions>
        </Dialog>
    );
}

const mapStatesToProps = state => state.loginReducer;

const mapDispatchToProps = {
    resetLogin, changeUserData, tryToLog, setError
};

export default connect(mapStatesToProps, mapDispatchToProps)((withStyles(styles)(Login)));