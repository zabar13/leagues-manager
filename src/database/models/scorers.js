'use strict';
module.exports = (sequelize, DataTypes) => {
    const Scorers = sequelize.define('Scorers', {
        season: DataTypes.STRING,
        time: DataTypes.INTEGER,
        name: DataTypes.STRING,
        nationality: DataTypes.STRING,
    }, {});
    Scorers.associate = function (models) {
        Scorers.belongsTo(models.Season)
    };
    return Scorers;
};