import getTeamStats from "../../../src/back/services/getTeamStats";

import db from '../../../src/server/config';

jest.mock('../../../src/server/config');

test('should team stats equal expected', async () => {
    db.get.mockImplementationOnce(() => ({
        data: [{
            corners: 2,
            goalsLost: 1,
            goalsScored: 1,
            id: 691,
            matchNo: 17,
            matchday: "matchday1",
            onTarget: 6,
            points: 1,
            possesion: 50,
            season: "Season test",
            shots: 10,
            side: "home",
            choosenTeam: "test"
        }, {
            corners: 4,
            goalsLost: 3,
            goalsScored: 0,
            id: 69,
            matchNo: 13,
            matchday: "matchday2",
            onTarget: 2,
            points: 0,
            possesion: 40,
            season: "Season test",
            shots: 5,
            side: "home",
            choosenTeam: "test"
        }, {
            corners: 3,
            goalsLost: 2,
            goalsScored: 1,
            id: 61,
            matchNo: 1,
            matchday: "matchday3",
            onTarget: 4,
            points: 3,
            possesion: 55,
            season: "Season test",
            shots: 7,
            side: "away",
            choosenTeam: "test"
        }, {
            corners: 5,
            goalsLost: 0,
            goalsScored: 1,
            id: 54,
            matchNo: 7,
            matchday: "matchday4",
            onTarget: 5,
            points: 3,
            possesion: 60,
            season: "Season test",
            shots: 8,
            side: "home",
            choosenTeam: "test"
        }, {
            corners: null,
            goalsLost: null,
            goalsScored: null,
            id: 540,
            matchNo: 21,
            matchday: "matchday5",
            onTarget: null,
            points: null,
            possesion: null,
            season: "Season test",
            shots: null,
            side: "away",
            choosenTeam: "test"
        }]
    }));

    expect.assertions(1);
    const teamStats = await getTeamStats({ choosenTeam: 'test', season: 'Season test' });

    const expected = {
        corners: 3.5,
        goalsLost: 1.5,
        goalsScored: 0.75,
        matchPlayed: 4,
        onTarget: 4.25,
        possesion: 51.25,
        shots: 7.5,
        choosenTeam: "test"
    };

    expect(teamStats).toEqual(expected);
})