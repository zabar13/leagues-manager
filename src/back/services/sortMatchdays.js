export default function sortMatchdays(matchdays) {
    return _.uniq(matchdays.map(item => {
        return item.matchday
            .replace('matchday', '')
    })
        .sort((a, b) => a - b)
        .map(item => `matchday${item}`));
}