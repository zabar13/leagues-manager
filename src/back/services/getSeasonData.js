import db from '../../server/config';

export default async function getSeasonData({ season, matchday, played }) {
    const tempMatchday = matchday ? matchday : 'Total';
    const tempPlayed = played ? played : false;

    const { data: seasonData } = await db.get(`api/fixtures/get/season/${season}/${tempMatchday}/${tempPlayed}`);

    return seasonData;
}