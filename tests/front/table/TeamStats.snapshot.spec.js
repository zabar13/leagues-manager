import React from 'react';
import { shallow } from 'enzyme';

import TeamStats from "../../../src/front/components/table/TeamStats";

test('renders correctly', () => {
    const tree = shallow(<TeamStats  choosenTeam={{ teamName: '' }}/>)
    expect(tree).toMatchSnapshot();
})
