import express from 'express';
import path from 'path';

import seasonsFacade from '../facade/seasons';
import scorersFacade from '../facade/scorers';
import carsdFacade from '../facade/cards';
import teamsFacade from '../facade/teams';
import usersFacade from '../facade/users';

const router = express.Router();

router.get('/api/fixtures/get', (request, response) => {
    return seasonsFacade.findAllSeasons(response);
});

router.post('/api/matches/:id', (request, response) => {
    return seasonsFacade.updateSeason(request, response);
})

router.get('/api/fixtures/get/season/:season/:matchday/:played', (request, response) => {
    return seasonsFacade.findSeason(request, response);
});

router.get('/api/fixtures/get/team/:teamName/:season/:matchday', (request, response) => {
    return seasonsFacade.findSeasonTeam(request, response);
});

router.get('/api/fixtures/get/matchday/:matchday/:season', (request, response) => {
    return seasonsFacade.findSeasonMatchday(request, response);
});

router.post('/api/fixtures/set', (request, response) => {
    return seasonsFacade.createSeason(request, response);
});

router.get('/api/season/count/:season', (request, response) => {
    return seasonsFacade.countPlayedMatches(request, response)
})

router.get('/api/scorers/count/:season', (request, response) => {
    return scorersFacade.countGoals(request, response)
})

router.post('/api/scorers', (request, response) => {
    return scorersFacade.createScorers(request, response);
});

router.delete('/api/scorers/delete/:id', (request, response) => {
    return scorersFacade.deleteScorer(request, response);
});

router.get('/api/scorers/get/:nationality/:season', (request, response) => {
    return scorersFacade.findSeasonScorersByNationality(request, response);
});

router.get('/api/scorers/get/:nationality/:season', (request, response) => {
    return scorersFacade.findSeasonScorersByNationality(request, response);
});

router.get('/api/scorers/all/:season', (request, response) => {
    return scorersFacade.findSeasonScorers(request, response);
});

router.get('/api/users/find/:login/:password', (request, response) => {
    return usersFacade.findUser(request, response);
});

router.post('/api/cards', (request, response) => {
    return carsdFacade.createCards(request, response);
});

router.get('/api/cards/:season', (request, response) => {
    return carsdFacade.findAllCards(request, response);
});

router.delete('/api/cards/delete/:id', (request, response) => {
    return carsdFacade.deleteCard(request, response);
});

router.post('/api/teams', (request, response) => {
    return teamsFacade.createTeams(request, response);
});

router.get('/api/teams/:season', (request, response) => {
    return teamsFacade.findSeasonTeams(request, response);
});

router.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../../dist/index.html'));
});

export default router;