export const ACTIONS = {
    SET_AVAILABLE_SEASONS: Symbol('SET_AVAILABLE_SEASONS'),
    SET_SEASON: Symbol('SET_SEASON')
};

const DEFAULT_STATE = {
    availableSeasons: [],
    chosenSeason: 'None',
    teams: [],
    seasonStats: {}
};

export default function mainReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.SET_AVAILABLE_SEASONS:
            return {
                ...state,
                availableSeasons: ['None', ...actions.availableSeasons]
            };
        case ACTIONS.SET_SEASON:
            return {
                ...state,
                chosenSeason: actions.chosenSeason,
                teams: actions.teams,
                seasonStats: actions.seasonStats
            };
        default:
            return state
    };
}