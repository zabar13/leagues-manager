import _ from 'lodash';

import { createMatchdaysArray } from "./createMatchdaysArray";
import createTable from "./createTable";

export default async function getTeamPositions({ choosenTeam, season, matchday }) {
    const matchdays = createMatchdaysArray(matchday);

    let teamPositions = [];

    for (let i = 0; i < matchdays.length; i++) {
        const tempTable = await createTable(season, matchdays[i]);
        const position = _.findIndex(tempTable, (team => team.team === choosenTeam));

        teamPositions = [...teamPositions,
        {
            matchday: matchdays[i].replace('matchday', ''),
            position: position + 1
        }]
    };

    return teamPositions;
}
