import React from 'react';
import { TextField, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    textFieldContainer: {
        width: '10%',
        margin:'5px 10px',
    }
};

export function EditMatchTextField({ id, value, onChange, classes }) {
    return (
        <Paper className={classes.textFieldContainer}>
            <TextField
                id={id}
                type='number'
                value={value || ''}
                onChange={(e) => onChange(e.target)} />
        </Paper>
    );
}

export default withStyles(styles)(EditMatchTextField)