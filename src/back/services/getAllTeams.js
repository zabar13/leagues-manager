import db from '../../server/config';

export default async function getAllTeams(season,first) {
    const { data:foundTeams } = await db.get(`api/teams/${season}`);

    const completeList = [...(foundTeams
        .map(item => { return item.name })
        .filter(team => team !== 'Bye')
        .sort())]

    return !first ? foundTeams : [first, ...completeList]
}