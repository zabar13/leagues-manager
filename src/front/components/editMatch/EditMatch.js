import React, {Fragment } from 'react';
import { connect } from 'react-redux';
import {
    Button, Paper, Dialog, DialogActions, DialogContent, DialogTitle
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { resetEditMatchDialog, changeStats, addPlayer, changePlayerStats, resetPlayer } from './actions/editMatchActionsCreators';
import { removePlayer } from './actions/editMatchActions';
import { updateFixtures } from '../fixtures/actions/fixturesActions';
import {
    MATCH_TEAM_STATS, MATCH_STATS_LABELS,
    ADD_SCORER_FIELDS, ADD_BOOKED_FIELDS
} from '../../../consts/const.json'
import AddEventPlayer from './AddEventPlayer';
import EventTypeList from './EventTypeList';
import EditMatchTextField from './EditMatchTextField';

const styles = {
    body: {
        '& >div': {
            display: 'flex',
            justifyContent: 'center'
        }
    },

    score: {
        justifyContent: 'center',
        fontSize: '26px',

        '& >div:first-child, & >div:nth-child(4)': {
            width: '30%',
            textAlign: 'center',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'

        },

        '& input': {
            fontSize: '26px',
            fontWeight: 'bold'
        }
    },

    stats: {
        flexDirection: 'column',
        marginTop: '20px',

        '& >div': {
            display: 'flex',
            justifyContent: 'center',

            '& >div:first-child, & >div:nth-child(4)': {
                fontSize: '20px',
                width: '30%',
                textAlign: 'center',
                margin: '5px 0',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            },

            '& >div:nth-child(4)': {
                visibility: 'hidden'
            }
        }
    }
}

export function EditMatch(props) {

    const {
        match, open, addPlayer, changePlayerStats, resetPlayer, removePlayer, classes
    } = props;

    const handleClose = () => {
        props.resetEditMatchDialog();
    };

    const onStatChange = (e, stat) => {
        if (e.value < 0) { e.value = 0 };
        const side = e.id === 'home' ? 'homeTeam' : 'awayTeam';
        props.changeStats(e.value, side, stat);
    }

    const onSaveClick = async () => {
        const { match, updateFixtures, matchday, resetEditMatchDialog, season, teams } = props;
        await updateFixtures({ match, matchday, season, teams });
        resetEditMatchDialog();
    }

    
    return (
        <Dialog
            maxWidth='md'
            disableBackdropClick={true}
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle >
                <div>EDIT MATCH</div>
            </DialogTitle>
            <DialogContent className={classes.body}>
                {match.matchNo ?
                    <Fragment >
                        <div className={classes.score}>
                            <Paper ><b>{match.homeTeam.teamName}</b></Paper>
                            <EditMatchTextField
                                id='home'
                                value={match.homeTeam.goalsScored}
                                onChange={(data) => onStatChange(data, 'goalsScored')}
                            />
                            <EditMatchTextField
                                id='away'
                                value={match.awayTeam.goalsScored}
                                onChange={(data) => onStatChange(data, 'goalsScored')}
                            />
                            <Paper ><b>{match.awayTeam.teamName}</b></Paper>
                        </div>
                        <div className={classes.stats}>
                            {MATCH_TEAM_STATS.map((stat, i) => {
                                return (
                                    <div key={stat}>
                                        <Paper >{MATCH_STATS_LABELS[i]}</Paper>
                                        <EditMatchTextField
                                            id='home'
                                            value={match.homeTeam[stat]}
                                            onChange={(data) => onStatChange(data, stat)}
                                        />
                                        <EditMatchTextField
                                            id='away'
                                            value={match.awayTeam[stat]}
                                            onChange={(data) => onStatChange(data, stat)}
                                        />
                                        <Paper ></Paper>
                                    </div>
                                )
                            })}

                        </div>
                        <div>SCORERS</div>

                        <AddEventPlayer
                            onChange={(value, stat) => changePlayerStats(value, stat, 'scorer')}
                            reset={() => resetPlayer('scorer')}
                            add={(value) => addPlayer(value, 'scorers')}
                            player={props.scorer}
                            stats={ADD_SCORER_FIELDS} />
                        <EventTypeList match={match} eventType={'scorers'}
                            onDeleteClick={(player, eventType) => removePlayer(player, eventType)} />

                        <div >CARDS</div>

                        <AddEventPlayer
                            onChange={(value, stat) => changePlayerStats(value, stat, 'booked')}
                            reset={() => resetPlayer('booked')}
                            player={props.booked}
                            add={(value) => addPlayer(value, 'cards')}
                            stats={ADD_BOOKED_FIELDS} />
                        <EventTypeList match={match} eventType={'cards'} onDeleteClick={(player, eventType) => removePlayer(player, eventType)} />
                    </Fragment>
                    : <div>Empty</div>}
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleClose}
                    color="inherit">
                    CANCEL
            </Button>
                <Button
                    color="inherit"
                    onClick={onSaveClick}>
                    SAVE
            </Button>
            </DialogActions>
        </Dialog>
    )
}

const mapStateToProps = state => state.editMatch;

const mapDispatchToProps = {
    resetEditMatchDialog, updateFixtures, changeStats,
    addPlayer, changePlayerStats, resetPlayer, removePlayer
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(EditMatch)))