import { deletePlayer } from "./editMatchActionsCreators";
import deletePlayerFromDB from "../../../../back/services/deletePlayerFromDB";

export function removePlayer(player, eventType) {
    return async dispatch => {
        let customId = player.customId;

        if (player.id) {
            await deletePlayerFromDB(player, eventType)
            customId = player.id;
        }

        dispatch(deletePlayer(customId, eventType))
    };
}