import React from 'react';
import { shallow } from 'enzyme';

import EditMatchTextField from '../../../src/front/components/editMatch/EditMatchTextField';

test('renders correctly', () => {
    const tree = shallow(<EditMatchTextField />);
    expect(tree).toMatchSnapshot();
})