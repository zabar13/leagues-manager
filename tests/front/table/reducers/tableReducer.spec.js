import deepFreeze from 'deep-freeze';

import tableReducer, { ACTIONS } from '../../../../src/front/components/table/reducers';

test('should set teams', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: false,
        choosenTeam: {},
        teamsNames: [],
        teamPositions: []
    };

    const action = {
        type: ACTIONS.SET_TEAMS,
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2']
    }

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: false,
        choosenTeam: {},
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: []
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set team', () => {
    const stateBefore = {
        team: 'None',
        openStats: false,
        openChart: false,
        teamPositions: [],
        choosenTeam: {},
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        matchdays: [],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.SET_TEAM,
        team: 'Test_2'
    }

    const stateAfter = {
        team: 'Test_2',
        openStats: false,
        openChart: false,
        choosenTeam: {},
        teamPositions: [],
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [],
        matchdays: [],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should open stat dialog and set choosen team', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: false,
        openChart: false,
        teamPositions: [],
        choosenTeam: {},
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [],
        matchdays: [],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.OPEN_STATS_DIALOG,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        }
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        teamPositions: [],
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [],
        matchdays: [],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should close stat dialog & reset choosenTeam ', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: [],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.CLOSE_STATS_DIALOG
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: false,
        openChart: false,
        teamPositions: [],
        choosenTeam: {},
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: [],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set matchdays', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: [],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.SET_MATCHDAYS,
        matchdays: ['matchday1', 'matchday2', 'matchday3']
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set matchday', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.SET_MATCHDAY,
        matchday: 'matchday1',
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'matchday1'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should open chart dialog & sets team positions', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.OPEN_CHART_DIALOG,
        teamPositions: [{ matchday: 1, position: 5 }]
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: true,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should close chart dialog', () => {
    const stateBefore = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: true,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [{ matchday: 1, position: 5 }],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    const action = {
        type: ACTIONS.CLOSE_CHART_DIALOG
    };

    const stateAfter = {
        teams: [],
        team: 'None',
        openStats: true,
        openChart: false,
        choosenTeam: {
            matchPlayed: 8,
            possesion: 44.63,
            shots: 7.25,
            teamName: 'Test_1'
        },
        teams: [
            { name: 'Test_1' },
            { name: 'Test_2' }],
        teamsNames: ['Test_1', 'Test_2'],
        teamPositions: [],
        matchdays: ['matchday1', 'matchday2', 'matchday3'],
        matchday: 'Total'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(tableReducer(stateBefore, action)).toEqual(stateAfter);
})