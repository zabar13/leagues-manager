import deepFreeze from 'deep-freeze';

import loginReducer, { ACTIONS } from "../../../../src/front/components/login/reducers/loginReducer";

test('should open login dialog', () => {
    const stateBefore = {
        open: false,
        logged: false
    };

    const action = {
        type: ACTIONS.OPEN_LOGIN
    };

    const stateAfter = {
        open: true,
        logged: false
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change logged status on true', () => {
    const stateBefore = {
        open: false,
        logged: false
    };

    const action = {
        type: ACTIONS.CHANGE_LOG_STATUS,
        logged: true
    };

    const stateAfter = {
        open: false,
        logged: true
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change user textfield data', () => {
    const stateBefore = {
        open: false,
        logged: false
    };

    const action = {
        type: ACTIONS.CHANGE_USER_DATA,
        value: 'test',
        data: 'xxx'
    };

    const stateAfter = {
        open: false,
        logged: false,
        xxx: 'test'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set error', () => {
    const stateBefore = {
        open: true,
        logged: false
    };

    const action = {
        type: ACTIONS.SET_ERROR,
        error: 'test error'

    };

    const stateAfter = {
        open: true,
        logged: false,
        error: 'test error'
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set user', () => {
    const stateBefore = {
        open: true,
        logged: false
    };

    const action = {
        type: ACTIONS.SET_USER,
        user: { name: 'admin', password: 'admin', role: 'admin' }

    };

    const stateAfter = {
        open: true,
        logged: false,
        user: { name: 'admin', password: 'admin', role: 'admin' }
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset login dialog', () => {
    const stateBefore = {
        open: true,
        logged: true,
        user: { name: 'admin', password: 'admin', role: 'admin' },
        error: 'test error'
    };

    const action = {
        type: ACTIONS.RESET_LOGIN
    };

    const stateAfter = {
        open: false,
        logged: true,
        user: { name: 'admin', password: 'admin', role: 'admin' }
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(loginReducer(stateBefore, action)).toEqual(stateAfter);
})