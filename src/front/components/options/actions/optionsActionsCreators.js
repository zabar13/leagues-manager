import { ACTIONS } from '../reducers/optionsReducer';

export function changeTeamData(value, field) {
    return {
        type: ACTIONS.UPDATE_TEAM_DATA,
        value,
        field
    };
}

export function resetTeam() {
    return {
        type: ACTIONS.RESET_TEAM
    };
}

export function addTeam(team) {
    return {
        type: ACTIONS.ADD_TEAM,
        team
    }
}

export function resetCreateSeason() {
    return {
        type: ACTIONS.RESET_CREATE_SEASON
    }
}

export function deleteTeam(id) {
    return {
        type: ACTIONS.DELETE_TEAM,
        id
    }
}
