import _ from 'lodash';

import db from '../../server/config';

export default async function getAvailableSeasons() {
    const { data: fixtures } = await db.get('api/fixtures/get');
    return _.uniq(fixtures.map(fixture => fixture.season));
}