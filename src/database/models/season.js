'use strict';
module.exports = (sequelize, DataTypes) => {
  const Season = sequelize.define('Season', {
    user: DataTypes.STRING,
    season: DataTypes.STRING,
    matchday: DataTypes.STRING,
    matchNo: DataTypes.INTEGER,
    side: DataTypes.STRING,
    teamName: DataTypes.STRING,
    goalsScored: DataTypes.INTEGER,
    goalsLost: DataTypes.INTEGER,
    shots: DataTypes.INTEGER,
    onTarget: DataTypes.INTEGER,
    possesion: DataTypes.INTEGER,
    corners: DataTypes.INTEGER,
    points: DataTypes.INTEGER
  }, {});
  Season.associate = function (models) {
    Season.hasMany(models.Scorers, { as: 'scorers' });
    Season.hasMany(models.Cards, { as: 'cards' });
  };
  return Season;
};