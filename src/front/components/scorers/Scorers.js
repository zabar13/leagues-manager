import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { setAvailableTeams, setChoosenScorers } from './actions/scorersActions';
import { changeTeam } from './actions/scorersActionsCreators';
import CustomSelect from '../common/CustomSelect';

const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        justifyContent: 'space-around',
        padding: '10px',

        '& >div:nth-child(2)': {
            width: '60%',
            display: 'flex',
            flexDirection: 'column',
            overflow: 'auto',
        }
    },

    column: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },

    row: {
        display: 'flex',
    },

    paper: {
        textAlign: 'center',
        fontSize: '18px',
        margin: '5px',
        width: '10%',
        display: 'inline-table',
    },

    paperPlayer: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        textAlign: 'center',
        fontSize: '18px',
        margin: '5px',
        width: '60%'
    }
}

export function Scorers({
    season, seasonAllTeams, setAvailableTeams, setChoosenScorers,
    scorers, team, teams, classes, changeTeam
}) {

    useEffect(() => {
        setAvailableTeams(season);
        setChoosenScorers(team, season, seasonAllTeams);
    },[team]);

    return (
        <div className={classes.root}>
            <CustomSelect
                label='SELECT TEAM'
                onChangeFunction={(selected) => changeTeam(selected)}
                dataArray={teams}
                value={team}
            />
            {scorers.length !== 0 ?
                <div>
                    {scorers.map((scorer, i) => {
                        const [, players] = scorer;
                        return (
                            <div className={classes.row} key={i}>
                                <Paper className={classes.paper}>{i + 1}</Paper>
                                <div className={classes.column}>
                                    {players.map((player, i) => {
                                        return (
                                            <div key={i} className={classes.row}>
                                                <Paper className={classes.paperPlayer}>{player.name}</Paper>
                                                <Paper className={classes.paper}>{player.nationality}</Paper>
                                                <Paper className={classes.paper}>{player.goals}</Paper>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        )
                    })}
                </div>
                : <div>
                    <Paper>NO SCORERS</Paper>
                </div>}
        </div>
    );
}

const mapStateToProps = state => {
    return {
        ...state.scorersReducer,
        season: state.mainReducer.chosenSeason,
        seasonAllTeams: state.mainReducer.teams
    };
}

const mapDispatchToProps = { setChoosenScorers, setAvailableTeams, changeTeam };

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(Scorers)))