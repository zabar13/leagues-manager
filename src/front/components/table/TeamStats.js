import React,{Fragment} from 'react';
import { connect } from 'react-redux';
import { DialogTitle, Dialog, DialogContent, DialogActions, Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { TEAM_STAT_LABELS, TEAM_STATS } from '../../../consts/const.json';
import { closeStatsDialog } from './actions/tableActionsCreators';
import { setChoosenTeamPositions, setChoosenTeamResults } from './actions/tableActions';

const styles = {
    body: {
        '& >div': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',

            '& div': {
                padding: '4px',
                margin: '5px',
                textAlign: 'center',
            },

            '& div:first-child': {
                width: '40%',
            },

            '& div:nth-child(2)': {
                width: '10%',
            },
        }
    }
}

export function TeamStats({ ...props }) {

    const onChartClick = async () => {
        const { choosenTeam, chosenSeason, matchday, setChoosenTeamPositions, matchdays } = props;

        await setChoosenTeamPositions({
            choosenTeam: choosenTeam.choosenTeam,
            season: chosenSeason,
            matchday: matchday !== 'Total' ? matchday : `matchday${matchdays.length}`
        });
    }

    const onResultsClick = async () => {
        const { choosenTeam, chosenSeason, matchday, setChoosenTeamResults, matchdays } = props;

        await setChoosenTeamResults({
            choosenTeam: choosenTeam.choosenTeam,
            season: chosenSeason,
            matchday: matchday !== 'Total' ? matchday : `matchday${matchdays.length}`
        });
    }

    const { choosenTeam, classes,
        closeStatsDialog, openStats } = props;
    return (
        <Dialog
            fullWidth
            disableBackdropClick={true}
            open={openStats}
            onClose={closeStatsDialog}
        >
            <DialogTitle>
                <div>{`${choosenTeam.choosenTeam} stats`}</div>
            </DialogTitle>
            <DialogContent className={classes.body}>
                <Fragment>
                    {TEAM_STATS.map((stat, i) => {
                        if (isNaN(choosenTeam[stat])) return null;
                        return (
                            <div key={i}>
                                <Paper >{TEAM_STAT_LABELS[i]}</Paper>
                                <Paper >{`${choosenTeam[stat]}`}</Paper>
                            </div>
                        )
                    })}<div>
                        <Button onClick={onChartClick}>Positions Chart</Button>
                        <Button onClick={onResultsClick}>Match Results</Button>
                    </div>
                </Fragment>
            </DialogContent>
            <DialogActions>
                <Button
                    variant='text'
                    onClick={closeStatsDialog}
                    color='inherit'>
                    OK
                    </Button>
            </DialogActions>
        </Dialog >

    );
}

const mapStateToProps = state => {
    return {
        ...state.table,
        chosenSeason: state.mainReducer.chosenSeason
    };
};

const mapDispatchToProps = {
    closeStatsDialog, setChoosenTeamPositions, setChoosenTeamResults
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(TeamStats)));
