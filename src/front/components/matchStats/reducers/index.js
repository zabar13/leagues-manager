export const ACTIONS = {
    OPEN_MATCH_STATS_DIALOG: Symbol('OPEN_MATCH_STATS_DIALOG'),
    RESET_MATCH_STATS_DIALOG: Symbol('RESET_MATCH_STATS_DIALOG')
};

const DEFAULT_STATE = {
    open: false,
    match: []
};

export default function matchStatsReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.OPEN_MATCH_STATS_DIALOG:
            return {
                ...state,
                open: true,
                match: actions.match
            };
        case ACTIONS.RESET_MATCH_STATS_DIALOG:
            return DEFAULT_STATE;
        default:
            return state
    };
}