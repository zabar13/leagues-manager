import React from 'react';
import { shallow } from 'enzyme';

import AddEventPlayer from "../../../src/front/components/editMatch/AddEventPlayer";

test('renders correctly', () => {
    const tree = shallow(<AddEventPlayer stats={[]}/>);
    expect(tree).toMatchSnapshot();
  })