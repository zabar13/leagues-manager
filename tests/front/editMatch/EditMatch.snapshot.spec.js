import React from 'react';
import { shallow } from 'enzyme';

import { EditMatch } from '../../../src/front/components/editMatch/EditMatch';

test('renders correctly', () => {
    const tree = shallow(<EditMatch match={{ matchNo: '' }} classes={{}} />);
    expect(tree).toMatchSnapshot();
})
