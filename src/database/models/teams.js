'use strict';
module.exports = (sequelize, DataTypes) => {
    const Teams = sequelize.define('Teams', {
        season: DataTypes.STRING,
        teamNo: DataTypes.INTEGER,
        name: DataTypes.STRING,
        short: DataTypes.STRING
    }, {});
    Teams.associate = function (models) {
    };
    return Teams;
};