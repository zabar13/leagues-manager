import React from 'react';
import { shallow } from 'enzyme';

import EventTypeList from '../../../src/front/components/editMatch/EventTypeList';

test('renders correctly', () => {
    const tree = shallow(<EventTypeList
        match={{
            stat: [{
                name: '', nationality: ''
            }]
        }} eventType={'stat'} />);
    expect(tree).toMatchSnapshot();
})

EventTypeList