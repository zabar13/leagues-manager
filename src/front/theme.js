import { createMuiTheme } from '@material-ui/core';

export const theme = createMuiTheme({
    typography: {
        suppressDeprecationWarnings: true
    },
    overrides: {
        MuiDialog: {
            paperWidthSm: {
                maxWidth: '900px'
            }
        },
        MuiDialogTitle: {
            root: {
                backgroundColor: '#081E30',
                color: '#ffffff',
                textAlign: 'center',
            }
        },

        MuiDialogContent: {
            root: {
                backgroundColor: '#E2E2E2',
            }
        },

        MuiDialogActions: {
            root: {
                backgroundColor: '#081E30',
                color: '#ffffff',
                textAlign: 'center',
            }
        },

        MuiFormHelperText: {
            root: {
                color: 'red',
            }
        },

        MuiInputBase: {
            inputSelect: {
                textAlign: 'center',
                paddingLeft: '10px'
            },
            input: {
                textAlign: 'center'
            }
        }
    }
});