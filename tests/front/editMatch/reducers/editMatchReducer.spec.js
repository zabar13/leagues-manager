import deepFreeze from 'deep-freeze';

import editMatchReducer, { ACTIONS } from '../../../../src/front/components/editMatch/reducers';

test('should open edit match dialog', () => {
    const stateBefore = {
        open: false,
        match: {},
        scorer: {},
        booked: {}
    };

    const action = {
        type: ACTIONS.OPEN_EDIT_MATCH_DIALOG,
        match: {
            awayTeam: {},
            homeTeam: {},
            matchNo: 15
        }
    };

    const stateAfter = {
        open: true,
        match: {
            awayTeam: {},
            homeTeam: {},
            matchNo: 15
        },
        scorer: {},
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should add player to event', () => {
    const stateBefore = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder' }]
        },
        scorer: {},
        booked: {}
    };

    const action = {
        type: ACTIONS.ADD_PLAYER,
        player: { name: 'J.Wick' },
        eventType: 'scorers'
    };

    const stateAfter = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [
                { name: 'R.Wilder' },
                { name: 'J.Wick' }]
        },
        scorer: {},
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change player data', () => {
    const stateBefore = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder' }]
        },
        scorer: { nationality: 'TE1' },
        booked: {}
    };

    const action = {
        type: ACTIONS.CHANGE_PLAYER_STATS,
        value: 'J.Clone',
        stat: 'name',
        eventType: 'scorer'
    };

    const stateAfter = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [
                { name: 'R.Wilder' }]
        },
        scorer: { name: 'J.Clone', nationality: 'TE1' },
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})
test('should delete player ', () => {
    const stateBefore = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder', id: 20 }]
        },
        scorer: {},
        booked: {}
    };

    const action = {
        type: ACTIONS.DELETE_PLAYER,
        customId: 20,
        eventType: 'scorers'
    };

    const stateAfter = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: []
        },
        scorer: {},
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset actual player data', () => {
    const stateBefore = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder' }]
        },
        scorer: {},
        booked: { nationality: 'TE1', name: 'R.Kilot' }
    };

    const action = {
        type: ACTIONS.RESET_PLAYER,
        eventType: 'booked'
    };

    const stateAfter = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [
                { name: 'R.Wilder' }]
        },
        scorer: {},
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should change team stat', () => {
    const stateBefore = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder' }]
        },
        scorer: { nationality: 'TE1' },
        booked: {}
    };

    const action = {
        type: ACTIONS.UPDATE_STAT,
        value: 3,
        side: 'awayTeam',
        stat: 'corners'
    };

    const stateAfter = {
        open: false,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3', corners: 3 },
            scorers: [
                { name: 'R.Wilder' }]
        },
        scorer: { nationality: 'TE1' },
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset edit match dialog', () => {
    const stateBefore = {
        open: true,
        match: {
            matchNo: 13,
            homeTeam: { name: 'Team_1' },
            awayTeam: { name: 'Team_3' },
            scorers: [{ name: 'R.Wilder' }]
        },
        scorer: { nationality: 'TE1' },
        booked: {}
    };

    const action = {
        type: ACTIONS.RESET_EDIT_MATCH_DIALOG
    };

    const stateAfter = {
        open: false,
        match: {},
        scorer: {},
        booked: {}
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(editMatchReducer(stateBefore, action)).toEqual(stateAfter);
})