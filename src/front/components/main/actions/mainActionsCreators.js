import { ACTIONS } from '../reducers';

export function setAvailableSeasons(availableSeasons) {
    return {
        type: ACTIONS.SET_AVAILABLE_SEASONS,
        availableSeasons
    };
}

export function setSeason({ chosenSeason, teams, seasonStats }) {
    return {
        type: ACTIONS.SET_SEASON,
        chosenSeason,
        teams,
        seasonStats
    };
}