import getAvailableSeasons from "../../../src/back/services/getAvailableSeasons";

import db from '../../../src/server/config';

jest.mock('../../../src/server/config');

test('should seasons list equal expected', async () => {
    db.get.mockImplementationOnce(() => ({
        data: [{
            id: 1,
            season: 'Season test'
        }, {
            id: 56,
            season: 'Season test'
        }, {
            id: 14,
            season: 'Season new'
        }, {
            id: 59,
            season: 'Season new'
        }, {
            id: 42,
            season: 'Season xxx'
        }]
    }));

    expect.assertions(1);

    const seasonsList = await getAvailableSeasons();

    const expected = ['Season test', 'Season new', 'Season xxx'];

    expect(seasonsList).toEqual(expected);
})