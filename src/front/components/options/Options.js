import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import CreateLeague from './CreateLeague';

const styles = {
    root: {
        position: 'relative',
        left: '120px',
        width: 'calc(100vw - 120px)',
        display: 'flex',
        flexDirection: 'column',
        padding: '20px 0',
    }
};

export function Options(props) {

    useEffect(() => {
        if (props.logged === false) {
            props.history.push("/");
        }
    }, [props.logged]);

    const { classes } = props;
    return (
        <div className={classes.root}>
            <CreateLeague />
        </div >
    );
}

const mapStateToProps = state => {
    return {
        logged: state.loginReducer.logged
    }
};

export default connect(mapStateToProps)((withStyles(styles)(Options)))