import React, { Fragment } from "react";
import { Paper } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        width: '100%',

        '& >div': {
            width: '50%',
            display: 'flex',
            flexDirection: 'column',

            '& >div': {
                display: "flex",
                alignItems: 'center',
                justifyContent:'center',

                '& div': {
                    margin: '5px 10px',
                    padding: '2px',
                    textAlign: 'center',
                    fontSize: '16px',
                    display: 'inline-table',
                },
            }
        },
    },

    paperName: {
        width: '40%',
    },

    paperTime: {
        width: '10%',
    },

    yellowCard: {
        backgroundColor: 'yellow',
    },

    redCard: {
        backgroundColor: 'red',
    },
}

export function MatchStastPlayersEvents({ homeTeam, awayTeam, events, classes }) {
    if (homeTeam[events].length === 0 && awayTeam[events].length === 0) return null;

    return (
        <div className={classes.root}>
            {homeTeam[events].length !== 0 ?
                <div>
                    {homeTeam[events].map((event, i) => {
                        const { colour = '', name, time } = event;
                        const card = `${colour.toLowerCase()}Card`;
                        const eventTime = `${time}\'`;
                        return (
                            <div key={i}>
                                <Paper className={classes.paperName}>{name}</Paper>
                                <Paper
                                    className={`${classes.paperTime} ${classes[card]}`}
                                >{eventTime}</Paper>
                            </div>
                        )
                    })}
                </div> : <div></div>
            }
            {awayTeam[events].length !== 0 ?
                <div >
                    {awayTeam[events].map((event, i) => {
                        const { colour = '', name, time } = event;
                        const card = `${colour.toLowerCase()}Card`;
                        const eventTime = `${time}\'`
                        return (
                            <div key={i}>
                                <Paper className={`${classes.paperTime} ${classes[card]}`}
                                >{eventTime}</Paper>
                                <Paper className={classes.paperName}>{name}</Paper>
                            </div>
                        )
                    })}
                </div> :
                <div></div>
            }
        </div>
    )
}

export default withStyles(styles)(MatchStastPlayersEvents)