import getAllTeams from "../../../../back/services/getAllTeams";
import { setScorers, setTeams } from "./scorersActionsCreators";
import getScorers from "../../../../back/services/getScorers";

export function setAvailableTeams(season) {
    return async dispatch => {
        const teams = await getAllTeams(season, 'All');
        dispatch(setTeams(teams))
    }
}

export function setChoosenScorers(team,season,seasonAllTeams) {
    return async dispatch => {
        const scorers = await getScorers(team,season,seasonAllTeams);
        dispatch(setScorers(scorers))
    }
}