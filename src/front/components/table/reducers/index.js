export const ACTIONS = {
    SET_TEAMS: Symbol('SET_TEAMS'),
    OPEN_STATS_DIALOG: Symbol('OPEN_STATS_DIALOG'),
    CLOSE_STATS_DIALOG: Symbol('CLOSE_STATS_DIALOG'),
    OPEN_CHART_DIALOG: Symbol('OPEN_CHART_DIALOG'),
    CLOSE_CHART_DIALOG: Symbol('CLOSE_CHART_DIALOG'),
    OPEN_RESULTS_DIALOG: Symbol('OPEN_RESULTS_DIALOG'),
    CLOSE_RESULTS_DIALOG: Symbol('CLOSE_RESULTS_DIALOG'),
    SET_PICKED_TEAM: Symbol('SET_PICKED_TEAM'),
    SET_MATCHDAYS: Symbol('SET_MATCHDAYS'),
    SET_MATCHDAY: Symbol('SET_MATCHDAY')
};

const DEFAULT_STATE = {
    teams: [],
    pickedTeam: 'None',
    openStats: false,
    openChart: false,
    teamPositions: [],
    choosenTeam: {},
    teamsNames: [],
    matchdays: [],
    matchday: 'Total',
    openResults: false,
    teamResults: []
};

export default function tableReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.SET_TEAMS:
            return {
                ...state,
                teams: actions.teams,
                teamsNames: actions.teamsNames
            };
        case ACTIONS.OPEN_STATS_DIALOG:
            return {
                ...state,
                openStats: true,
                choosenTeam: actions.choosenTeam
            };
        case ACTIONS.SET_PICKED_TEAM:
            return {
                ...state,
                pickedTeam: actions.pickedTeam
            };
        case ACTIONS.SET_MATCHDAYS:
            return {
                ...state,
                matchdays: [...actions.matchdays]
            };
        case ACTIONS.SET_MATCHDAY:
            return {
                ...state,
                matchday: actions.matchday
            };
        case ACTIONS.CLOSE_STATS_DIALOG:
            return {
                ...state,
                openStats: false,
                choosenTeam: {},
                
            };
        case ACTIONS.OPEN_CHART_DIALOG:
            return {
                ...state,
                openChart: true,
                teamPositions: actions.teamPositions
            };
        case ACTIONS.CLOSE_CHART_DIALOG:
            return {
                ...state,
                openChart: false,
                teamPositions: []
            };
        case ACTIONS.OPEN_RESULTS_DIALOG:
            return {
                ...state,
                openResults: true,
                teamResults: actions.teamResults
            };
        case ACTIONS.CLOSE_RESULTS_DIALOG:
            return {
                ...state,
                openResults: false,
                teamResults: []
            };
        default:
            return state
    };
}