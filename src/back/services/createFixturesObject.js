import _ from 'lodash';

import { fixtures } from '../../consts/fixtures.json';
import getAlreadyExistMatchdays from './getAlreadyExistMatchdays';
import setTeamsInDB from './setTeamsInDB.js';

export default async function createFixturesObject({ teams, rounds, leagueName, user }) {
    const { data: createdTeams } = await setTeamsInDB(teams, leagueName);
    const existedMatchdays = await getAlreadyExistMatchdays(leagueName);
    const matches = translateFixtures(createdTeams, fixtures[`teams${teams.length}`])
    let completeSeason;

    if (rounds == '1 ROUND') {
        completeSeason = matches;
    } else {
        completeSeason = createSecondRound(matches);
    }

    const matchdays = Object.keys(completeSeason);


    const matchDaysToAdd = _.difference(matchdays, existedMatchdays);

    if (matchDaysToAdd.length == 0) {
        return null
    }

    return _.flattenDeep(matchDaysToAdd.map(matchday => {
        return completeSeason[matchday].map(match => {
            const { matchNo, homeTeam, awayTeam } = match;
            return [
                {
                    side: 'home',
                    teamName: homeTeam,
                    matchNo,
                    matchday,
                    season: leagueName,
                    user: user.login
                },
                {
                    side: 'away',
                    teamName: awayTeam,
                    matchNo,
                    matchday,
                    season: leagueName,
                    user: user.login
                }
            ];
        });
    }));
}

function createSecondRound(matches) {
    let twoRoundMatches = { ...matches }
    const firstRoundMatchdays = Object.keys(matches);
    const firstRoundMatchdaysNo = Object.keys(matches).length;

    firstRoundMatchdays.map((match, i) => {

        const secondRoundMatches = matches[match].map(match => {
            const { homeTeam, awayTeam } = match;
            return {
                ...match,
                homeTeam: awayTeam,
                awayTeam: homeTeam
            }
        })
        twoRoundMatches = {
            ...twoRoundMatches,
            [`matchday${firstRoundMatchdaysNo + i + 1}`]: secondRoundMatches
        }
    });

    return twoRoundMatches;
}

function translateFixtures(teams, fixtures) {
    let matchdays = {};
    Object.keys(fixtures).map(matchday => {
        const translatedFixtures = fixtures[matchday].map(fixture => {
            const homeTeam = (teams.filter(team => team.teamNo == fixture[0]))[0].name;
            const awayTeam = (teams.filter(team => team.teamNo == fixture[1]))[0].name;
            return {
                homeTeam,
                awayTeam,
                matchNo: fixture[2]
            }
        })
        return matchdays = {
            ...matchdays,
            [matchday]: [...translatedFixtures]
        }
    })
    return matchdays;
}

