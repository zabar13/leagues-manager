import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { REACT_LINKS, LEFT_MENU_BUTTONS_LABELS } from '../../consts/const.json';

const styles = {
    menu: {
        position: 'fixed',
        top: '100px',
        color: 'white',
        width: '120px',
        display: 'flex',
        flexDirection: 'column',
        height: 'calc(100% - 100px)',
        backgroundColor: '#081E30',

        '& a': {
            textDecoration: 'none',
        },

        '& button': {
            margin: '10px',
            minWidth: '100px',
        }
    },
    
    hidden: {
        display: 'none'
    }
}

export function Menu({ chosenSeason, logged, classes }) {

    function onLinkClick(e) {
        if (chosenSeason === 'None') e.preventDefault();
    }

    return (
        <div className={classes.menu}>
            {REACT_LINKS.map((link, i) => {
                const disabled = ((chosenSeason === 'None') && ((i >= 1) && (i <= 3))) ? true : false;
                return (
                    <NavLink key={i}
                        className={!logged && i > 3 ? classes.hidden : ''}
                        to={link}
                        activeStyle={{ color: '#45a29e' }}
                        onClick={((i > 0) && (i < 4)) ? onLinkClick : () => { }}>
                        <Button
                            variant='contained'
                            disabled={disabled}>
                            {LEFT_MENU_BUTTONS_LABELS[i]}
                        </Button>
                    </NavLink>
                )
            })}

        </div >
    );
};

const mapStateToProps = state => {
    return {
        chosenSeason: state.mainReducer.chosenSeason,
        logged: state.loginReducer.logged
    }
};

export default connect(mapStateToProps)((withStyles(styles)(Menu)))