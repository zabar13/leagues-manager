import deepFreeze from 'deep-freeze';

import matchStatsReducer, { ACTIONS } from '../../../../src/front/components/matchStats/reducers';

test('should open match stat dialog and set match data', () => {
    const stateBefore = {
        open: false,
        match: []
    };

    const action = {
        type: ACTIONS.OPEN_MATCH_STATS_DIALOG,
        match: { matchNo: 2, homeTeam: { name: 'Team_1' }, awayTeam: { name: 'Team_2' } }
    };

    const stateAfter = {
        open: true,
        match: { matchNo: 2, homeTeam: { name: 'Team_1' }, awayTeam: { name: 'Team_2' } }
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(matchStatsReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset match stat dialog', () => {
    const stateBefore = {
        open: true,
        match: { matchNo: 2, homeTeam: { name: 'Team_1' }, awayTeam: { name: 'Team_2' } }
    };

    const action = {
        type: ACTIONS.RESET_MATCH_STATS_DIALOG
    };

    const stateAfter = {
        open: false,
        match: []
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(matchStatsReducer(stateBefore, action)).toEqual(stateAfter);
})