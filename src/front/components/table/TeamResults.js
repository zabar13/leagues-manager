import React,{Fragment} from 'react';
import { connect } from 'react-redux';
import { DialogTitle, Dialog, DialogContent, DialogActions, Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { closeResultsDialog } from './actions/tableActionsCreators';
import { checkMatchBackgroundcolour } from './actions/tableActions';

const styles = {
    body: {
        '& >div': {
            display: 'flex',
            justifyContent: 'center',

            '& >div': {
                padding: '5px 10px',
                textAlign: 'center',
                margin: '5px'
            },

            '& >:first-child, & >:nth-child(4)': {
                width: '30%',
            },

            '& >:nth-child(2), & >:nth-child(3)': {
                width: '5%',
            }
        }
    },
    yellow: {
        '& >div': {
            backgroundColor: '#FEFDA0'
        },
    },

    green: {
        '& >div': {
            backgroundColor: '#A2FFA2'
        }
    },

    red: {
        '& >div': {
            backgroundColor: '#FFD5D6'
        }
    },

};

export function TeamResults({ openResults, choosenTeam, classes, teamResults, closeResultsDialog }) {

    return (
        <Dialog
            fullWidth
            disableBackdropClick={true}
            open={openResults}
            onClose={closeResultsDialog}
            scroll='body'
        >
            <DialogTitle>
                <div>{`${choosenTeam.choosenTeam} results`}</div>
            </DialogTitle>
            <DialogContent className={classes.body}>
                <Fragment>
                    {teamResults.map((result, i) => {
                        const { home, homeGoals, away, awayGoals } = result;
                        const colour = checkMatchBackgroundcolour(result, choosenTeam.choosenTeam);
                        return (
                            <div className={classes[colour]} key={i}>
                                <Paper>
                                    {home == choosenTeam.choosenTeam
                                        ? <b>{home}</b> : home}</Paper>
                                <Paper>{homeGoals}</Paper>
                                <Paper>{awayGoals}</Paper>
                                <Paper>
                                    {away == choosenTeam.choosenTeam
                                        ? <b>{away}</b> : away}</Paper>
                            </div>
                        )
                    })}
                </Fragment>
            </DialogContent>
            <DialogActions>
                <Button
                    variant='text'
                    onClick={closeResultsDialog}
                    color='inherit'>
                    OK
                    </Button>
            </DialogActions>
        </Dialog >

    );
}

const mapStateToProps = state => {
    return {
        ...state.table
    };
};

const mapDispatchToProps = {
    closeResultsDialog
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(TeamResults)));
