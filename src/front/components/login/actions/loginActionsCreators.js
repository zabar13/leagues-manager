import { ACTIONS } from '../reducers/loginReducer';

export function resetLogin() {
    return {
        type: ACTIONS.RESET_LOGIN
    };
}

export function changeUserData(value, data) {
    return {
        type: ACTIONS.CHANGE_USER_DATA,
        value,
        data
    };
}

export function openLogin() {
    return {
        type: ACTIONS.OPEN_LOGIN
    };
}

export function changeLogStatus(logged) {
    return {
        type: ACTIONS.CHANGE_LOG_STATUS,
        logged
    };
}

export function setUser(user) {
    return {
        type: ACTIONS.SET_USER,
        user
    };
}

export function setError(error) {
    return {
        type: ACTIONS.SET_ERROR,
        error
    };
}