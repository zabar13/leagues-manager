import React from 'react';
import { shallow } from 'enzyme';

import { MatchStats } from '../../../src/front/components/matchStats/MatchStats';

test('renders correctly', () => {
    const tree = shallow(<MatchStats
        match={{
            homeTeam: { scorers: [] },
            awayTeam: { scorers: [] }
        }}  classes={{}}/>);
    expect(tree).toMatchSnapshot();
})

